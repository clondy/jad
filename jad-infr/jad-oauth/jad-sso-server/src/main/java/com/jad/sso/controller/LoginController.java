/**
 * Copyright (c) 2011-2014, hubin (243194995@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.jad.sso.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jad.commons.web.waf.request.WafRequestWrapper;
import com.jad.sso.SSOConfig;
import com.jad.sso.SSOHelper;
import com.jad.sso.SSOToken;
import com.jad.sso.Token;
import com.jad.sso.annotation.Action;
import com.jad.sso.annotation.Login;
import com.jad.web.mvc.BaseController;

/**
 * 登录
 */
//@Controller
public class LoginController extends BaseController {
	
	@Autowired
	private SessionDAO sessionDAO;
	
//	@Autowired
//	private SystemService systemService;
	
	/**
	 * 登录 （注解跳过权限验证）
	 */
	@Login(action = Action.Skip)
	@RequestMapping("/login")
	public String login(Model model,HttpServletRequest request, HttpServletResponse response) {
		String returnUrl = request.getParameter(SSOConfig.getInstance().getParamReturl());
		Token token = SSOHelper.getToken(request);
		if (token == null) {
			/**
			 * 正常登录 需要过滤sql及脚本注入
			 */
			WafRequestWrapper wr = new WafRequestWrapper(request);
			String name = wr.getParameter("userid");
			if (name != null && !"".equals(name)) {
				/*
				 * 设置登录 Cookie
				 * 最后一个参数 true 时添加 cookie 同时销毁当前 JSESSIONID 创建信任的 JSESSIONID
				 */
				SSOToken st = new SSOToken(request, "1000");
				SSOHelper.setSSOCookie(request, response, st, true);

				// 重定向到指定地址 returnUrl
				if (StringUtils.isBlank(returnUrl)) {
					returnUrl = "/index.html";
				} else {
					
					try {
						returnUrl = URLDecoder.decode(returnUrl, SSOConfig.getSSOEncoding());
					} catch (UnsupportedEncodingException e) {
						logger.error("encodeRetURL error." + returnUrl);
					}
				}
				return redirectTo(returnUrl);
			} else {
				if (StringUtils.isNotBlank(returnUrl)) {
					model.addAttribute("ReturnURL", returnUrl);
				}
				return "login";
			}
		} else {
			if (StringUtils.isBlank(returnUrl)) {
				returnUrl = "/index.html";
			}
			return redirectTo(returnUrl);
		}
	}

	/**
	 * 登录 （注解跳过权限验证）
	 */
	@Login(action = Action.Skip)
	@RequestMapping("/loginpost")
	public String loginpost(HttpServletRequest request, HttpServletResponse response) {
		/**
		 * 生产环境需要过滤sql注入
		 */
		WafRequestWrapper req = new WafRequestWrapper(request);
		String username = req.getParameter("username");
		String password = req.getParameter("password");
		if ("kisso".equals(username) && "123".equals(password)) {
			
			/*
			 * authSSOCookie 设置 cookie 同时改变 jsessionId
			 */
			SSOToken st = new SSOToken(request);
			st.setId(12306L);
			st.setUid("12306");
			st.setType(1);
			
			//记住密码，设置 cookie 时长 1 周 = 604800 秒 【动态设置 maxAge 实现记住密码功能】
			//String rememberMe = req.getParameter("rememberMe");
			//if ( "on".equals(rememberMe) ) {
			//	request.setAttribute(SSOConfig.SSO_COOKIE_MAXAGE, 604800); 
			//}
			SSOHelper.setSSOCookie(request, response, st, true);
			
			/*
			 * 登录需要跳转登录前页面，自己处理 ReturnURL 使用 
			 * HttpUtil.decodeURL(xx) 解码后重定向
			 */
			return redirectTo("/index.html");
		}
		return "login";
	}
}
