/**
 * Copyright (c) 2011-2014, hubin (243194995@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.jad.sso.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jad.sso.SSOConfig;
import com.jad.sso.SSOHelper;
import com.jad.sso.SSOToken;
import com.jad.web.mvc.BaseController;

/**
 * 首页
 */
@Controller
public class IndexController extends BaseController {

	/**
	 * <p>
	 * SSOHelper.getToken(request) 
	 * 
	 * 从 Cookie 解密 token 使用场景，拦截器
	 * </p>
	 * 
	 * <p>
	 * SSOHelper.attrToken(request)
	 * 
	 * 非拦截器使用减少二次解密
	 * </p>
	 */
	@RequestMapping("/index")
	public String index(Model model,HttpServletRequest request, HttpServletResponse response) {
		
		SSOToken st = SSOHelper.attrToken(request);
		if (st != null) {
			System.err.println(" Long 类型 ID: " + st.getId());
			model.addAttribute("userId", st.getUid());
		}
		System.err.println(" 启动注入测试模式：" + SSOConfig.getInstance().getRunMode());
		return "index";
	}
	

}
