package com.jad.commons.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.repository.NoRepositoryBean;

import com.jad.commons.entity.TreeEo;
import com.jad.commons.qo.TreeQo;
import com.jad.commons.util.QoUtil;
import com.jad.dao.AbstractJadEntityDao;
import com.jad.dao.entity.EoMetaInfo;
import com.jad.dao.utils.EntityUtils;

@NoRepositoryBean
public class AbstractTreeDao<EO extends TreeEo<EO,ID>, ID extends Serializable> 
	extends AbstractJadEntityDao<EO,ID> implements TreeDao<EO,ID> {
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public int updateSort(ID id,Integer sort) {
		
		List params=new ArrayList();
		params.add(sort);
		params.add(id);
		
		String sql="UPDATE " + tableName() + " SET sort = ? WHERE id = ?";
	
		return super.executeSql(sql,params);
	}
	
//	@SuppressWarnings({ "rawtypes", "unchecked" })
//	public List<EO> findByParentIdsLike(String parentIds){
//		
//		Class<EO> entityClass = getEntityClass();
//		
//		List params=Lists.newArrayList();
//		
//		String alias = entityClass.getSimpleName();
//		
//		StringBuffer sb = new StringBuffer();
//		sb.append(SqlHelper.getSelectSql(getEntityClass(), alias, new ArrayList()));
//		
//		sb.append(" where ");
//		sb.append(alias).append(".parent_ids ");
//		sb.append("like '%?%' and ");
//		sb.append(alias).append(".del_flag=? ");
//		
//		params.add(parentIds);
//		params.add(Constants.DEL_FLAG_NORMAL);
//		
//		return super.findBySql(sb.toString(), params);
//		
//		
//	}
	
	/**
	 * 查询子节点列表
	 * @param parentId
	 * @return
	 */
	public List<EO>findSubList(ID parentId){
		TreeQo<ID> qo=new TreeQo<ID>();
		qo.setParentIdsLike(","+parentId+",");
		List<EO> list = findByQo(QoUtil.wrapperNormalDataQo(qo));
		return list;
	}
	

	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public int updateParentIds(ID id,ID parentId,String parentIds) {
		
		String tableName = tableName();
		
		List params=new ArrayList();
		
		params.add(parentId);
		params.add(parentIds);
		params.add(id);
		
		String sql="UPDATE " + tableName + " SET  parent_id = ?,  parent_ids = ? WHERE id = ? ";
		
		return super.executeSql(sql,params);
		
		
	}
	
	private String tableName(){
		EoMetaInfo<EO> ei=EntityUtils.getEoInfo(getEntityClass());
		return ei.getTableName();
	}
	
	

}
