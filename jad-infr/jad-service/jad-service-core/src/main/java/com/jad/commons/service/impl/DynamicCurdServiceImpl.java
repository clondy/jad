package com.jad.commons.service.impl;

import org.springframework.transaction.annotation.Transactional;

import com.jad.commons.vo.BaseVo;
import com.jad.commons.vo.ValueObject;
import com.jad.dao.JadEntityDao;
import com.jad.dao.entity.EoMetaInfo;
import com.jad.dao.entity.VoMetaInfo;


@SuppressWarnings("rawtypes")
@Transactional(readOnly = true)
public class DynamicCurdServiceImpl extends AbstractServiceImpl  {
	
	private EoMetaInfo eoMetaInfo;	
	
	private VoMetaInfo voMetaInfo;
	
	private JadEntityDao dao;
	
	@SuppressWarnings("unchecked")
	public void afterPropertiesSet() throws Exception{
		curdHelper.setEoMetaInfo(eoMetaInfo);
		curdHelper.setVoMetaInfo(voMetaInfo);
		curdHelper.setDao(dao);
	}

	public void setEoMetaInfo(EoMetaInfo eoMetaInfo) {
		this.eoMetaInfo = eoMetaInfo;
	}

	public void setVoMetaInfo(VoMetaInfo voMetaInfo) {
		this.voMetaInfo = voMetaInfo;
	}

	public void setDao(JadEntityDao dao) {
		this.dao = dao;
	}

	
	
}
