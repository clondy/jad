package com.jad.commons.service;

import java.io.Serializable;

import com.jad.commons.vo.EntityObject;
import com.jad.commons.vo.BaseVo;

/**
 * 可缓存的service
 * @author Administrator
 *
 */
public interface CacheableService<EO extends EntityObject, ID extends Serializable,VO extends BaseVo<ID>> 
		extends BaseService{

	/**
	 * 是否启用缓存
	 * @return
	 */
	boolean isOpenCache();
	
//	/**
//	 * 是否缓存列表
//	 * @return
//	 */
//	public boolean isCacheList();
	
	/**
	 * 获取单条数据
	 * @param id
	 * @return
	 */
	public VO findByIdFromCache(ID id);
	
//	public VoListCo<VO,ID> findByIdListFromCache(List<ID> idList);
//	public VoListCo<VO,ID> findByIdListFromCache(List<ID> idList,String orderBy);
	
	/**
	 * 查询列表数据
	 * @param entity
	 * @return
	 */
//	public <QO extends QueryObject> VoListCo<VO,ID> findListFromCache(QO qo); 
//	public <QO extends QueryObject> VoListCo<VO,ID> findListFromCache(QO qo,String orderBy); 
//	public List<VO> findAllFromCache();
	
	/**
	 * 查询分页数据
	 * @param page 分页对象
	 * @param vo
	 * @return
	 */
//	public <QO extends QueryObject> PageListCo<VO,ID> findPageFromCache(Page<VO> page, QO qo);
//	public <QO extends QueryObject> PageListCo<VO,ID> findPageFromCache(Page<VO> page, QO qo,String orderBy);
	
	
	
	/**
	 * 获取单条数据
	 * @param id
	 * @return
	 */
	public VO findByIdFromDb(ID id);
//	public List<VO> findByIdListFromDb(List<ID> idList);
//	public List<VO> findByIdListFromDb(List<ID> idList,String orderBy);
	
	/**
	 * 查询列表数据
	 * @param entity
	 * @return
	 */
//	public <QO extends QueryObject> List<VO> findListFromDb(QO qo); 
//	public <QO extends QueryObject> List<VO> findListFromDb(QO qo,String orderBy); 
//	public List<VO> findAllFromDb();
	
	/**
	 * 查询分页数据
	 * @param page 分页对象
	 * @param vo
	 * @return
	 */
//	public <QO extends QueryObject> Page<VO> findPageFromDb(Page<VO> page, QO qo);
//	public <QO extends QueryObject> Page<VO> findPageFromDb(Page<VO> page, QO qo,String orderBy);
	
	
	
	
}
