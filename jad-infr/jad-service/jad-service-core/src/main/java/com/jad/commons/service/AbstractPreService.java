package com.jad.commons.service;

import java.io.Serializable;
import java.util.Date;

import org.springframework.transaction.annotation.Transactional;

import com.jad.commons.context.Constants;
import com.jad.commons.service.impl.CurdHelper;
import com.jad.commons.utils.IdGen;
import com.jad.commons.utils.StringUtils;
import com.jad.commons.vo.BaseVo;
import com.jad.commons.vo.EntityObject;
import com.jad.dao.entity.EoMetaInfo;

@Transactional(readOnly = true)
public abstract class AbstractPreService<EO extends EntityObject,ID extends Serializable> 
	implements BaseService{
	
	public abstract ID genId();

	protected <VO extends BaseVo<ID>> void preInsert(VO vo,EoMetaInfo<EO> eoMetaInfo) {
		if(vo==null)return;
		if(vo.getId() ==null 
				|| StringUtils.isBlank(vo.getId().toString())){
			vo.setId(genId());
		}
		
		if(vo.getCreateDate() == null ){
			vo.setCreateDate(new Date());	
		}
		if(vo.getUpdateDate() == null ){
			vo.setUpdateDate(new Date());
		}
		if(vo.getDelFlag()==null){
			vo.setDelFlag(Constants.DEL_FLAG_NORMAL);
		}
		
	}
	

	protected <VO extends BaseVo<ID>> void preUpdate(VO vo) {
		if(vo==null)return;
		if(vo.getUpdateDate() == null ){
			vo.setUpdateDate(new Date());
		}
		
	}
	
	
}
