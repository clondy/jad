package com.jad.core.sys.dao.impl;

import com.jad.core.sys.dao.LogDao;
import com.jad.core.sys.entity.LogEo;
import com.jad.dao.AbstractJadEntityDao;
import com.jad.dao.annotation.JadDao;

@JadDao("logDao")
public class LogDaoImpl extends AbstractJadEntityDao<LogEo,String>implements LogDao{
}
