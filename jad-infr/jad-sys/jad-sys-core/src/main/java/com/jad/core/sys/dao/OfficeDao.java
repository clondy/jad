/**
 */
package com.jad.core.sys.dao;

import java.util.List;

import com.jad.commons.dao.TreeDao;
import com.jad.core.sys.entity.OfficeEo;
import com.jad.dao.annotation.JadDao;

/**
 * 机构DAO接口
 */
@JadDao
public interface OfficeDao extends TreeDao<OfficeEo,String> {
	
	/**
	 * 跟据角色id查找部门
	 * @param roleId
	 * @return
	 */
	public List<OfficeEo> findByRoleId(String roleId);
	
}
