package com.jad.core.sys.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAttribute;

import org.hibernate.validator.constraints.Length;

import com.jad.commons.entity.BaseEo;


@Entity
@Table(name = "sys_dict")
public class DictEo extends BaseEo<String>{

	private static final long serialVersionUID = 1L;
	
	private String value;	// 数据值
	
	private String label;	// 标签名
	
	private String type;	// 类型
	
	private String description;// 描述
	
	private Integer sort;	// 排序
	
	private String parentId;//父Id

	public DictEo() {
		super();
	}
	
	public DictEo(String id){
		super(id);
	}
	
	public DictEo(String value, String label){
		this.value = value;
		this.label = label;
	}
	
	@Column(name="value")
	@XmlAttribute
	@Length(min=1, max=100)
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	@Column(name="label")
	@XmlAttribute
	@Length(min=1, max=100)
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Column(name="type")
	@Length(min=1, max=100)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name="description")
	@XmlAttribute
	@Length(min=0, max=100)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name="sort")
	@OrderBy("sort asc,create_Date desc ")
	@NotNull
	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	@Column(name="parent_id")
	@Length(min=1, max=100)
	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	
	
}
