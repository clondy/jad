package com.jad.core.sys.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;

import com.jad.commons.entity.BaseEo;
import com.jad.dao.annotation.RelateColumn;
import com.jad.dao.enums.RelateLoadMethod;


@Entity
@Table(name = "sys_role")
public class RoleEo extends BaseEo<String>{
	
	private static final long serialVersionUID = 1L;
	
	private OfficeEo office;	// 归属机构
	private String name; 	// 角色名称
	private String enname;	// 英文名称
	private String roleType;// 权限类型
	private String dataScope;// 数据范围
	
	private String sysData; 		//是否是系统数据
	private String useable; 		//是否是可用
	
	public RoleEo() {
		super();
	}
	
	public RoleEo(String id){
		super(id);
	}
	

	@Column(name="useable")
	public String getUseable() {
		return useable;
	}

	public void setUseable(String useable) {
		this.useable = useable;
	}

	@Column(name="is_sys")
	public String getSysData() {
		return sysData;
	}

	public void setSysData(String sysData) {
		this.sysData = sysData;
	}
	
	@ManyToOne
	@RelateColumn(name="office_id",loadMethod=RelateLoadMethod.LEFT_JOIN)
	public OfficeEo getOffice() {
		return office;
	}

	public void setOffice(OfficeEo office) {
		this.office = office;
	}

	@Column(name="name")
	@Length(min=1, max=100)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name="enname")
	@Length(min=1, max=100)
	public String getEnname() {
		return enname;
	}

	public void setEnname(String enname) {
		this.enname = enname;
	}
	
	@Column(name="role_type")
	@Length(min=1, max=100)
	public String getRoleType() {
		return roleType;
	}

	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

	@Column(name="data_scope")
	public String getDataScope() {
		return dataScope;
	}

	public void setDataScope(String dataScope) {
		this.dataScope = dataScope;
	}



}
