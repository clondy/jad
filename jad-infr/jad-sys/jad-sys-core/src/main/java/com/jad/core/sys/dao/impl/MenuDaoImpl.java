package com.jad.core.sys.dao.impl;

import java.util.ArrayList;
import java.util.List;

import com.jad.commons.context.Constants;
import com.jad.commons.dao.AbstractTreeDao;
import com.jad.core.sys.constant.RoleConstant;
import com.jad.core.sys.dao.MenuDao;
import com.jad.core.sys.entity.MenuEo;
import com.jad.dao.annotation.JadDao;
import com.jad.dao.entity.EoMetaInfo;
import com.jad.dao.utils.EntityUtils;
import com.jad.dao.utils.SqlHelper;

@JadDao("menuDao")
public class MenuDaoImpl extends AbstractTreeDao<MenuEo,String>implements MenuDao{

	
	/**
	 * 跟据角色id查找有权限的菜单
	 * @param roleId
	 * @return
	 */
	public List<MenuEo> findByRoleId(String roleId) {
		
		EoMetaInfo<MenuEo> ei=EntityUtils.getEoInfo(MenuEo.class);
		List params= new ArrayList();
		String alias="a";
		StringBuffer sb=new StringBuffer();
		
		sb.append("select ");
		
		sb.append(SqlHelper.getSelectColumn(ei,alias));
		
		sb.append(" from ");
		
		sb.append(SqlHelper.getSelectFrom(ei,alias,params));
		sb.append(" JOIN sys_role_menu rm ON rm.menu_id = a.id"); 
		
		sb.append(" WHERE a.del_flag =? and rm.role_id=? ");
		
		sb.append(" ORDER BY a.sort,a.update_date desc ");
		
		params.add(Constants.DEL_FLAG_NORMAL);
		params.add(roleId);
		return super.findBySql(sb.toString(), params, MenuEo.class);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<MenuEo> findByUserId(String userId) {
		
		EoMetaInfo<MenuEo> ei=EntityUtils.getEoInfo(MenuEo.class);
		
		List params= new ArrayList();
		String alias="a";
		
		StringBuffer sb=new StringBuffer();
		
		sb.append("select DISTINCT ");
		
		sb.append(SqlHelper.getSelectColumn(ei,alias));
		
		sb.append(" from ");
		
		sb.append(SqlHelper.getSelectFrom(ei,alias,params));
		
		sb.append(" JOIN sys_role_menu rm ON rm.menu_id = a.id");
		sb.append(" JOIN sys_role r ON r.id = rm.role_id AND r.useable=? ");
		
		sb.append(" JOIN sys_user_role ur ON ur.role_id = r.id");
		sb.append(" JOIN sys_user u ON u.id = ur.user_id AND u.id =? ");
		
		sb.append(" WHERE a.del_flag =? AND r.del_flag = ? AND u.del_flag = ?");
		
		sb.append(" ORDER BY a.sort,a.update_date desc ");
		
		params.add(RoleConstant.USEABLE);
		params.add(userId);
		params.add(Constants.DEL_FLAG_NORMAL);
		params.add(Constants.DEL_FLAG_NORMAL);
		params.add(Constants.DEL_FLAG_NORMAL);
		
		return super.findBySql(sb.toString(), params, MenuEo.class);
	}


	


}
