/**
 */
package com.jad.core.sys.dao;

import java.util.List;

import com.jad.core.sys.entity.RoleEo;
import com.jad.core.sys.vo.MenuVo;
import com.jad.core.sys.vo.OfficeVo;
import com.jad.dao.JadEntityDao;
import com.jad.dao.annotation.JadDao;

/**
 * 角色DAO接口
 */
@JadDao
public interface RoleDao extends JadEntityDao<RoleEo,String> {


	/**
	 * 查询用户所属角色
	 * @param userId
	 * @return
	 */
	public List<RoleEo> findByUserId(String userId);
	

	/**
	 * 维护角色与菜单权限关系
	 * @param role
	 * @return
	 */
	public int deleteRoleMenu(String roleId);
	
	public int insertRoleMenu(String roleId,List<MenuVo> menuList);
	
	/**
	 * 维护角色与公司部门关系
	 * @param role
	 * @return
	 */
	public int deleteRoleOffice(String roleId);

	public int insertRoleOffice(String roleId,List<OfficeVo> officeList);

}
