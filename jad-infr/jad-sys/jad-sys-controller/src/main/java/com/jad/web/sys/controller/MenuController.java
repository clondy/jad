/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.web.sys.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.jad.commons.annotation.ApiCurdType;
import com.jad.commons.context.Global;
import com.jad.commons.enums.CurdType;
import com.jad.commons.security.shiro.SecurityHelper;
import com.jad.commons.util.TreeUtil;
import com.jad.commons.utils.StringUtils;
import com.jad.commons.web.api.Result;
import com.jad.core.sys.qo.MenuQo;
import com.jad.core.sys.service.MenuService;
import com.jad.core.sys.util.MenuUtil;
import com.jad.core.sys.vo.MenuVo;
import com.jad.web.mvc.BaseController;

/**
 * 菜单Controller
 * @author ThinkGem
 * @version 2013-3-23
 */
@Controller
@RequestMapping(value = "${adminPath}/sys/menu")
@Api(description = "菜单管理")
public class MenuController extends BaseController {

	@Autowired
	private MenuService menuService;
	
//	@ModelAttribute("menu")
//	public MenuVo get(@RequestParam(required=false) String id) {
//		if (StringUtils.isNotBlank(id)){
//			return menuService.findById(id);
//		}else{
//			return new MenuVo();
//		}
//	}
	
	
	
	/**
	 * 查看单条记录
	 * @param id
	 * @return
	 */
	@RequiresPermissions("sys:menu:view")
	@RequestMapping(value = {"find"})
	@ResponseBody
	@ApiOperation(value = "查看菜单信息",httpMethod = "GET")
	@ApiCurdType(CurdType.FIND_ONE)
	public Result<MenuVo> find(@RequestParam(required=true) String id){
		return Result.newSuccResult(menuService.findById(id));
	}
	
	/**
	 * 查询列表
	 * @return
	 */
	@RequiresPermissions("sys:menu:view")
	@ResponseBody
	@RequestMapping(value = {"findList"})
	@ApiOperation(value = "查询菜单列表",httpMethod = "GET")
	@ApiCurdType(CurdType.FIND_LIST)
	public Result<List<MenuVo>> findList(){
		List<MenuVo> list =  new ArrayList<MenuVo>();
		List<MenuVo> sourcelist = menuService.findList(new MenuQo());
		TreeUtil.sortList(list, sourcelist, MenuUtil.getRootId(), true);
		return Result.newSuccResult(list);
	}
	
	/**
	 * 修改
	 * @param menuVo
	 * @return
	 */
	@RequiresPermissions("sys:menu:edit")
	@RequestMapping(value = "update")
	@ResponseBody
	@ApiOperation(value = "修改菜单信息",httpMethod = "POST")
	@ApiCurdType(CurdType.UPDATE)
	public Result<?> update(MenuVo menuVo){
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		if (!beanValidator(result, menuVo)){
			return result;
		}
		if(StringUtils.isBlank(menuVo.getId())){
			result.setParamFail("修改失败,末知的id");
			return result;
		}
		preUpdate(menuVo,SecurityHelper.getCurrentUserId());
		menuService.update(menuVo);
		return result;
	}
	
	/**
	 * 新增
	 * @param userVo
	 * @return
	 */
	@RequiresPermissions("sys:menu:edit")
	@RequestMapping(value = "add")
	@ResponseBody
	@ApiOperation(value = "新增会员信息",httpMethod = "POST")
	@ApiCurdType(CurdType.ADD)
	public Result<?> add(MenuVo menuVo){
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		if (!beanValidator(result, menuVo)){
			return result;
		}
		preInsert(menuVo,SecurityHelper.getCurrentUserId());
		menuService.add(menuVo);
		return result;
	}
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@RequiresPermissions("sys:menu:edit")
	@RequestMapping(value = "del")
	@ResponseBody
	@ApiOperation(value = "删除菜单信息",httpMethod = "POST")
	@ApiCurdType(CurdType.DELETE)
	public Result<?> delete(@RequestParam(required=true)String id){
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		MenuVo delVo = new MenuVo();
		delVo.setId(id);
		preUpdate(delVo,SecurityHelper.getCurrentUserId());
		menuService.delete(delVo);
		return result;
	}
	
	

	@RequiresPermissions("sys:menu:view")
	@RequestMapping(value = {"list", ""})
	public String list(Model model) {
		List<MenuVo> list =  new ArrayList();
		List<MenuVo> sourcelist = menuService.findList(new MenuQo());
		TreeUtil.sortList(list, sourcelist, MenuUtil.getRootId(), true);
        model.addAttribute("list", list);
		return "modules/sys/menuList";
	}

	@RequiresPermissions("sys:menu:view")
	@RequestMapping(value = "form")
	public String form(MenuQo menuQo, Model model) {
		MenuVo menuVo = new MenuVo();
		
		
		if(StringUtils.isNotBlank(menuQo.getId())){//修改
			
			menuVo = menuService.findById(menuQo.getId());
			
		}else if(StringUtils.isNotBlank(menuQo.getParentId())){//添加下级
			
			menuVo.setParent(menuService.findById(menuQo.getParentId()));
			
		}else{ //添加新的
			
		}
		
//		if (menuVo.getParent()==null||menuVo.getParent().getId()==null){
//			menuVo.setParent(new MenuVo(MenuUtil.getRootId()));
//		}
//		menuVo.setParent(menuService.findById(menuVo.getParent().getId()));
//		// 获取排序号，最末节点排序号+30
//		if (StringUtils.isBlank(menuVo.getId())){
//			List<MenuVo> list = Lists.newArrayList();
//			List<MenuVo> sourcelist = menuService.findList(new MenuQo());
//			TreeUtil.sortList(list, sourcelist, menuVo.getParentId(), false);
//			if (list.size() > 0){
//				menuVo.setSort(list.get(list.size()-1).getSort() + 30);
//			}
//		}
		model.addAttribute("menuVo", menuVo);
		return "modules/sys/menuForm";
	}
	
	@RequiresPermissions("sys:menu:edit")
	@RequestMapping(value = "save")
	public String save(MenuVo menuVo, Model model, RedirectAttributes redirectAttributes) {
//		if(!systemService.getUser().isAdmin()){
//			addMessage(redirectAttributes, "越权操作，只有超级管理员才能添加或修改数据！");
//			return "redirect:" + adminPath + "/sys/role/?repage";
//		}
		if(Global.isDemoMode()){
			addMessage(redirectAttributes, "演示模式，不允许操作！");
			return "redirect:" + adminPath + "/sys/menu/";
		}
		if (!beanValidator(model, menuVo)){
//			return form(menuVo, model);
			return "modules/sys/menuForm";
		}
//		menuService.saveOrUpdate(menuVo);
		
		if(StringUtils.isNotBlank(menuVo.getId())){
			preUpdate(menuVo,SecurityHelper.getCurrentUserId());
			menuService.update(menuVo);
		}else{
			preInsert(menuVo,SecurityHelper.getCurrentUserId());
			menuService.add(menuVo);
		}
		
		addMessage(redirectAttributes, "保存菜单'" + menuVo.getName() + "'成功");
		return "redirect:" + adminPath + "/sys/menu/";
	}
	
	@RequiresPermissions("sys:menu:edit")
	@RequestMapping(value = "delete")
	public String delete(@RequestParam(required=true)String id, RedirectAttributes redirectAttributes) {
		if(Global.isDemoMode()){
			addMessage(redirectAttributes, "演示模式，不允许操作！");
			return "redirect:" + adminPath + "/sys/menu/";
		}
//		if (Menu.isRoot(id)){
//			addMessage(redirectAttributes, "删除菜单失败, 不允许删除顶级菜单或编号为空");
//		}else{
		
		MenuVo delVo = new MenuVo();
		delVo.setId(id);
		preUpdate(delVo,SecurityHelper.getCurrentUserId());
		
		menuService.delete(delVo);
			addMessage(redirectAttributes, "删除菜单成功");
//		}
		return "redirect:" + adminPath + "/sys/menu/";
	}

	@RequiresPermissions("user")
	@RequestMapping(value = "tree")
	public String tree() {
		return "modules/sys/menuTree";
	}

	@RequiresPermissions("user")
	@RequestMapping(value = "treeselect")
	public String treeselect(String parentId, Model model) {
		model.addAttribute("parentId", parentId);
		return "modules/sys/menuTreeselect";
	}
	
	/**
	 * 批量修改菜单排序
	 */
	@RequiresPermissions("sys:menu:edit")
	@RequestMapping(value = "updateSort")
	public String updateSort(@RequestParam(required=true)String[] ids, @RequestParam(required=true)Integer[] sorts, RedirectAttributes redirectAttributes) {
		if(Global.isDemoMode()){
			addMessage(redirectAttributes, "演示模式，不允许操作！");
			return "redirect:" + adminPath + "/sys/menu/";
		}
    	for (int i = 0; i < ids.length; i++) {
    		MenuVo menu = new MenuVo(ids[i]);
    		menu.setSort(sorts[i]);
    		menuService.updateSort(ids[i],sorts[i]);
    	}
    	addMessage(redirectAttributes, "保存菜单排序成功!");
		return "redirect:" + adminPath + "/sys/menu/";
	}
	
	/**
	 * isShowHide是否显示隐藏菜单
	 * @param extId
	 * @param isShowHidden
	 * @param response
	 * @return
	 */
	@RequiresPermissions("user")
	@ResponseBody
	@RequestMapping(value = "treeData")
	public List<Map<String, Object>> treeData(@RequestParam(required=false) String extId,@RequestParam(required=false) String isShowHide, HttpServletResponse response) {
		List<Map<String, Object>> mapList =  new ArrayList();
		List<MenuVo> list = menuService.findList(new MenuQo());
		for (int i=0; i<list.size(); i++){
			MenuVo e = list.get(i);
			if (StringUtils.isBlank(extId) || (extId!=null && !extId.equals(e.getId()) && e.getParentIds().indexOf(","+extId+",")==-1)){
				if(isShowHide != null && isShowHide.equals("0") && e.getIsShow().equals("0")){
					continue;
				}
				Map<String, Object> map = new HashMap();
				map.put("id", e.getId());
				map.put("pId", e.getParentId());
				map.put("name", e.getName());
				mapList.add(map);
			}
		}
		return mapList;
	}
}
