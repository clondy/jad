/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.web.sys.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jad.commons.annotation.ApiCurdType;
import com.jad.commons.enums.CurdType;
import com.jad.commons.vo.Page;
import com.jad.commons.vo.PageQo;
import com.jad.commons.web.api.Result;
import com.jad.core.sys.qo.LogQo;
import com.jad.core.sys.service.LogService;
import com.jad.core.sys.vo.LogVo;
import com.jad.web.mvc.BaseController;

/**
 * 日志Controller
 * @author ThinkGem
 * @version 2013-6-2
 */
@Controller
@RequestMapping(value = "${adminPath}/sys/log")
@Api(description = "日志管理")
public class LogController extends BaseController {

	@Autowired
	private LogService logService;
	
	@RequiresPermissions("sys:log:view")
	@RequestMapping(value = {"list", ""})
	public String list(LogQo logQo, HttpServletRequest request, HttpServletResponse response, Model model) {
        Page<LogVo> page = logService.findLogPage(getPage(request, response), logQo); 
        model.addAttribute("page", page);
		return "modules/sys/logList";
	}
	
	/**
	 * 查询列表
	 * @param userQo
	 * @param pageQo
	 * @return
	 */
	@RequiresPermissions("sys:log:view")
	@ResponseBody
	@RequestMapping(value = {"findPage"})
	@ApiOperation(value = "查询日志列表",httpMethod = "GET")
	@ApiCurdType(CurdType.FIND_LIST)
	public Result<Page<LogVo>> findPage(LogQo logQo,PageQo pageQo){
		Page<LogVo> page = logService.findPage(pageQo, logQo);
		return Result.newSuccResult(page);
	}
	

}



