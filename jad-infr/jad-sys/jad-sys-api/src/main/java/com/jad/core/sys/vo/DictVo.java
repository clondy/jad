/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.sys.vo;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAttribute;

import org.hibernate.validator.constraints.Length;

import com.jad.commons.annotation.CURD;
import com.jad.commons.vo.BaseVo;





/**
 * 字典Entity
 * @version 2013-05-15
 */
public class DictVo extends BaseVo<String>{

	private static final long serialVersionUID = 1L;
	
	@CURD(label="数据值",required=true)
	private String value;	// 数据值
	
	@CURD(label="标签名",required=true)
	private String label;	// 标签名
	
	@CURD(label="类型",required=true)
	private String type;	// 类型
	
	@CURD(label="描述",required=true)
	private String description;// 描述
	
	@CURD(label="排序",required=true)
	private Integer sort;	// 排序
	
	@CURD(label="父Id",required=true)
	private String parentId;//父Id

	public DictVo() {
		super();
	}
	
	
	public DictVo(String value, String label){
		this.value = value;
		this.label = label;
	}
	
	@XmlAttribute
	@Length(min=1, max=100)
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	@XmlAttribute
	@Length(min=1, max=100)
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Length(min=1, max=100)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@XmlAttribute
	@Length(min=0, max=100)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@NotNull
	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	@Length(min=1, max=100)
	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	
}

