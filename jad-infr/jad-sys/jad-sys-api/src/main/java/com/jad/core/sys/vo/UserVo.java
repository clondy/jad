/**
 */
package com.jad.core.sys.vo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jad.commons.annotation.CURD;
import com.jad.commons.vo.BaseVo;
import com.jad.core.sys.util.UserUtil;

/**
 * 用户Entity
 */
public class UserVo extends BaseVo<String> {
	
	private static final long serialVersionUID = 1L;
	private OfficeVo company;	// 归属公司
	private OfficeVo office;	// 归属部门
	
	@CURD(label="登录名")
	private String loginName;// 登录名
	
	@CURD(label="密码")
	private String password;// 密码
	
	@CURD(label="工号")
	private String no;		// 工号
	
	@CURD(label="姓名")
	private String name;	// 姓名
	
	@CURD(label="邮箱")
	private String email;	// 邮箱
	
	@CURD(label="电话")
	private String phone;	// 电话
	
	@CURD(label="手机")
	private String mobile;	// 手机
	
	@CURD(label="用户类型")
	private String userType;// 用户类型
	
	@CURD(label="最后登陆IP")
	private String loginIp;	// 最后登陆IP
	
	@CURD(label="最后登陆日期")
	private Date loginDate;	// 最后登陆日期
	
	@CURD(label="是否允许登陆")
	private String loginFlag;	// 是否允许登陆
	
	@CURD(label="头像")
	private String photo;	// 头像
	
	@CURD(label="原登录名")
	private String oldLoginName;// 原登录名
	
	@CURD(label="新密码")
	private String newPassword;	// 新密码
	
	@CURD(label="上次登陆IP")
	private String oldLoginIp;	// 上次登陆IP
	
	@CURD(label="上次登陆日期")
	private Date oldLoginDate;	// 上次登陆日期
	
//	private RoleVo role;	// 根据角色查询用户条件
	
	private List<RoleVo> roleList = new ArrayList<RoleVo>(); // 拥有角色列表
	
	private List<MenuVo> menuList = new ArrayList<MenuVo>();//拥有菜单列表

	public UserVo() {
		super();
	}
	
	public UserVo(String id){
		super(id);
	}

	public UserVo(String id, String loginName){
		super(id);
		this.loginName = loginName;
	}
	
	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getLoginFlag() {
		return loginFlag;
	}

	public void setLoginFlag(String loginFlag) {
		this.loginFlag = loginFlag;
	}

	@JsonIgnore
	public OfficeVo getCompany() {
		return company;
	}

	public void setCompany(OfficeVo company) {
		this.company = company;
	}
	
	@JsonIgnore
	public OfficeVo getOffice() {
		return office;
	}

	public void setOffice(OfficeVo office) {
		this.office = office;
	}

	@Length(min=1, max=100, message="登录名长度必须介于 1 和 100 之间")
	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	@JsonIgnore
	@Length(min=1, max=100, message="密码长度必须介于 1 和 100 之间")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Length(min=1, max=100, message="姓名长度必须介于 1 和 100 之间")
	public String getName() {
		return name;
	}
	
	@Length(min=1, max=100, message="工号长度必须介于 1 和 100 之间")
	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Email(message="邮箱格式不正确")
	@Length(min=0, max=200, message="邮箱长度必须介于 1 和 200 之间")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	@Length(min=0, max=200, message="电话长度必须介于 1 和 200 之间")
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Length(min=0, max=200, message="手机长度必须介于 1 和 200 之间")
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	
	@Length(min=0, max=100, message="用户类型长度必须介于 1 和 100 之间")
	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}


	public String getLoginIp() {
		return loginIp;
	}

	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getLoginDate() {
		return loginDate;
	}

	public void setLoginDate(Date loginDate) {
		this.loginDate = loginDate;
	}

	public String getOldLoginName() {
		return oldLoginName;
	}

	public void setOldLoginName(String oldLoginName) {
		this.oldLoginName = oldLoginName;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getOldLoginIp() {
		if (oldLoginIp == null){
			return loginIp;
		}
		return oldLoginIp;
	}

	public void setOldLoginIp(String oldLoginIp) {
		this.oldLoginIp = oldLoginIp;
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getOldLoginDate() {
		if (oldLoginDate == null){
			return loginDate;
		}
		return oldLoginDate;
	}

	public void setOldLoginDate(Date oldLoginDate) {
		this.oldLoginDate = oldLoginDate;
	}

	@JsonIgnore
	public List<MenuVo> getMenuList() {
		return menuList;
	}

	public void setMenuList(List<MenuVo> menuList) {
		this.menuList = menuList;
	}

	@JsonIgnore
	public List<RoleVo> getRoleList() {
		return roleList;
	}
	
	public void setRoleList(List<RoleVo> roleList) {
		this.roleList = roleList;
	}

	@JsonIgnore
	public List<String> getRoleIdList() {
		List<String> roleIdList = new ArrayList();
		for (RoleVo role : roleList) {
			if(StringUtils.isNotBlank(role.getId())){
				roleIdList.add(role.getId().toLowerCase());
			}
			
		}
		return roleIdList;
	}

	public void setRoleIdList(List<String> roleIdList) {
		roleList = new ArrayList();
		for (String roleId : roleIdList) {
			RoleVo role = new RoleVo();
			role.setId(roleId);
			roleList.add(role);
		}
	}
	
	
	/**
	 * 用户拥有的角色名称字符串, 多个角色名称用','分隔.
	 */
	@JsonIgnore
	public String getRoleNames() {
//		return Collections3.extractToString(roleList, "name", ",");
		if(roleList==null || roleList.isEmpty()){
			return "";
		}
		StringBuffer sb = new StringBuffer();
		for(int i=0;i<roleList.size();i++){
			RoleVo vo = roleList.get(i);
			if(i>0){
				sb.append(",");
			}
			sb.append(vo.getName());
		}
		
		return sb.toString();
	}
	@JsonIgnore
	public boolean isAdmin(){
		return UserUtil.isAdmin(this.id);
	}
	


	

	
}