package com.jad.core.sys.constant;

public class RoleConstant {

	/**
	 * 角色可用
	 */
	public static final String USEABLE = "1";
	
	/**
	 * 角色不可用
	 */
	public static final String DIS_USEABLE = "0";
	
	
}
