package com.jad.core.sys.service;

import java.util.List;

import com.jad.commons.service.TreeService;
import com.jad.core.sys.vo.MenuVo;

public interface MenuService extends TreeService<MenuVo,String>{
	
	
	public List<MenuVo> findByUserId(String userId);
	
	/**
	 * 跟据角色id查找有权限的菜单
	 * @param roleId
	 * @return
	 */
	public List<MenuVo> findByRoleId(String roleId);
	
	

}
