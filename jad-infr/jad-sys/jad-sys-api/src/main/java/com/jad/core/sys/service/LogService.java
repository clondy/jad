/**
 * Copyright &copy; 2012-2013 <a href="httparamMap://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.sys.service;

import com.jad.commons.context.RequestLogContext;
import com.jad.commons.service.CurdService;
import com.jad.commons.vo.Page;
import com.jad.commons.vo.PageQo;
import com.jad.core.sys.qo.LogQo;
import com.jad.core.sys.vo.LogVo;

/**
 * 日志Service
 */
public interface LogService extends CurdService<LogVo,String>  {

	public Page<LogVo> findLogPage(PageQo page, LogQo log);
	
	public void saveLog(RequestLogContext request, String title,boolean isAsync);
	
	public void saveLog(RequestLogContext request , Exception ex, String title);
	
	public void saveLogWidthAsynchronous(RequestLogContext request , Exception ex, String title);
	
	
}
