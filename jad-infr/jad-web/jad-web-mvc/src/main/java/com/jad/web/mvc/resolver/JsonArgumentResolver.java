package com.jad.web.mvc.resolver;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import com.jad.commons.json.JsonMapper;
import com.jad.commons.utils.StringUtils;
import com.jad.web.mvc.annotation.JsonArg;


/*
 <mvc:annotation-driven>
       <mvc:argument-resolvers>
        <bean class="com.jad.web.mvc.resolver.JsonArgumentResolver" />
    </mvc:argument-resolvers>
 </mvc:annotation-driven>
 */
public class JsonArgumentResolver implements HandlerMethodArgumentResolver {

	public static final String JSONBODYATTRIBUTE = "JSON_REQUEST_BODY";

	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		return parameter.hasParameterAnnotation(JsonArg.class);
	}

	@Override
	public Object resolveArgument(MethodParameter parameter,
			ModelAndViewContainer mavContainer, NativeWebRequest webRequest,
			WebDataBinderFactory binderFactory) throws Exception {

		String body = getRequestBody(webRequest);

		String arg = parameter.getParameterAnnotation(JsonArg.class).value();

		if (StringUtils.isEmpty(arg)) {
			arg = parameter.getParameterName();
		}
		Object val=JsonMapper.fromJsonString(body, parameter.getParameterType());
		return val;
	}

	private String getRequestBody(NativeWebRequest webRequest) {
		HttpServletRequest servletRequest = webRequest
				.getNativeRequest(HttpServletRequest.class);

		String jsonBody = (String) webRequest.getAttribute(JSONBODYATTRIBUTE,
				NativeWebRequest.SCOPE_REQUEST);
		if (jsonBody == null) {
			try {
				jsonBody = IOUtils.toString(servletRequest.getInputStream());
				webRequest.setAttribute(JSONBODYATTRIBUTE, jsonBody,
						NativeWebRequest.SCOPE_REQUEST);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
		return jsonBody;
	}

}
