package com.jad.web.mvc.swagger.conf;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import springfox.documentation.spi.service.contexts.Defaults;
import springfox.documentation.spring.web.DocumentationCache;
import springfox.documentation.spring.web.ObjectMapperConfigurer;
import springfox.documentation.spring.web.json.JacksonModuleRegistrar;
import springfox.documentation.spring.web.json.JsonSerializer;
import springfox.documentation.swagger2.configuration.Swagger2JacksonModule;

/**
 * api 配置
 * 
 * 需要在 spring配置文件中配置一个bean
 * 而且，这个bean实例必须配置到跟 srpign mvc 所在的context一致的context中
 * 
 * @author hechuan
 * 
 */
@EnableWebMvc
@JadEnableSwagger2
public class ApiSwaggerConfig implements BeanDefinitionRegistryPostProcessor{
//	public static final String API_SESSION_ID ="SwaggerApiSessionId";
	/**
	 * 标题
	 */
	private String title="JAD-API";
	
	/** 
	 * 描述
	 */
	private String description="这是jad自动生成的api";
	
	/**
	 * 团队地址
	 */
	private String termsOfServiceUrl="www.baidu.com";
	
	/**
	 * license
	 */
	private String license="Apache 2.0";
	
	/**
	 * licenseUrl
	 */
	private String licenseUrl="http://www.apache.org/licenses/LICENSE-2.0.html";
	
	/**
	 * version
	 */
	private String version="1.0.0";
	
	/**
	 * 联系信息
	 */
	private String contactName="hechuan";
	private String contactUrl="www.baidu.com";
	private String contactEmail="54hechuan@163.com";
	
	private List<ApiDocketConf>docketList;
  
	@Override
	public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
		registryModelAttributeParameterExpander(registry);
		registryOperationParameterReader(registry);
		registryDocket(registry);
	}
	
	private void registryModelAttributeParameterExpander(BeanDefinitionRegistry registry){
		String beanName = "modelAttributeParameterExpander";
		if (!registry.containsBeanDefinition(beanName)) {
			RootBeanDefinition beanDefinition=new RootBeanDefinition(JadModelAttributeParameterExpander.class);
			beanDefinition.setScope(BeanDefinition.SCOPE_SINGLETON);
			beanDefinition.setLazyInit(false);
			beanDefinition.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
			registry.registerBeanDefinition(beanName, beanDefinition);
		}
	}
	
	private void registryOperationParameterReader(BeanDefinitionRegistry registry){
		String beanName = "operationParameterReader";
		if (!registry.containsBeanDefinition(beanName)) {
			RootBeanDefinition beanDefinition=new RootBeanDefinition(JadOperationParameterReader.class);
			beanDefinition.setScope(BeanDefinition.SCOPE_SINGLETON);
			beanDefinition.setLazyInit(false);
			beanDefinition.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
			registry.registerBeanDefinition(beanName, beanDefinition);
		}
	}
	private void registryDocket(BeanDefinitionRegistry registry){
		
		
		if(docketList == null || docketList.isEmpty() ){
			return;
		}
		for(ApiDocketConf conf:docketList){
			
			ApiDocketConf apiConf = buildApiDocketConf(conf); 
			
			String beanName = "_api_docket_" + apiConf.getGroupName();
			if (!registry.containsBeanDefinition(beanName)) {
				RootBeanDefinition beanDefinition=new RootBeanDefinition(ApiDocketFactoryBean.class);
				beanDefinition.setScope(BeanDefinition.SCOPE_SINGLETON);
				beanDefinition.setLazyInit(false);
				beanDefinition.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
				beanDefinition.getPropertyValues().add("apiDocketConf", apiConf);
				registry.registerBeanDefinition(beanName, beanDefinition);
			}
		}
	}


	
	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
	}

	private ApiDocketConf buildApiDocketConf(ApiDocketConf conf){
		ApiDocketConf newConf = new ApiDocketConf();
		
		newConf.setGroupName(conf.getGroupName());
		if(StringUtils.isBlank(conf.getGroupName())){
			throw new RuntimeException("ApiDocketConf配置错误,groupName不能为空");
		}
		newConf.setPath(conf.getPath());
		
		if(StringUtils.isBlank(conf.getTitle())){
			newConf.setTitle(title );
		}else{
			newConf.setTitle(conf.getTitle());
		}
		if(StringUtils.isBlank(conf.getDescription())){
			newConf.setDescription(description );
		}else{
			newConf.setDescription(conf.getDescription());
		}
		if(StringUtils.isBlank(conf.getTermsOfServiceUrl())){
			newConf.setTermsOfServiceUrl(termsOfServiceUrl );
		}else{
			newConf.setTermsOfServiceUrl(conf.getTermsOfServiceUrl());
		}
		if(StringUtils.isBlank(conf.getLicense())){
			newConf.setLicense(license);
		}else{
			newConf.setLicense(conf.getLicense());
		}
		if(StringUtils.isBlank(conf.getLicenseUrl())){
			newConf.setLicenseUrl(licenseUrl);
		}else{
			newConf.setLicenseUrl(conf.getLicenseUrl());
		}
		if(StringUtils.isBlank(conf.getVersion())){
			newConf.setVersion(version);
		}else{
			newConf.setVersion(conf.getVersion());
		}
		if(StringUtils.isBlank(conf.getContactName())){
			newConf.setContactName(contactName);
		}else{
			newConf.setContactName(conf.getContactName());
		}
		if(StringUtils.isBlank(conf.getContactUrl())){
			newConf.setContactUrl(contactUrl);
		}else{
			newConf.setContactUrl(conf.getContactUrl());
		}
		if(StringUtils.isBlank(conf.getContactEmail())){
			newConf.setContactEmail(contactEmail);
		}else{
			newConf.setContactEmail(conf.getContactEmail());
		}
		
		return newConf ;
	}
	
	
	public void setTitle(String title) {
		this.title = title;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public void setTermsOfServiceUrl(String termsOfServiceUrl) {
		this.termsOfServiceUrl = termsOfServiceUrl;
	}


	public void setLicense(String license) {
		this.license = license;
	}


	public void setLicenseUrl(String licenseUrl) {
		this.licenseUrl = licenseUrl;
	}


	public void setVersion(String version) {
		this.version = version;
	}


	public void setContactName(String contactName) {
		this.contactName = contactName;
	}


	public void setContactUrl(String contactUrl) {
		this.contactUrl = contactUrl;
	}


	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}



	public void setDocketList(List<ApiDocketConf> docketList) {
		this.docketList = docketList;
	}
	
	@Bean
	public JacksonModuleRegistrar swagger2Module() {
	   return new Swagger2JacksonModule();
	}
	
	@Bean
	public JadApiExpandedParameterBuilderPlugin jadExpandedParameterBuilderPlugin() {
	   return new JadApiExpandedParameterBuilderPlugin();
	}
	

	  @Bean
	  public Defaults defaults() {
	    return new Defaults();
	  }

	  @Bean
	  public DocumentationCache resourceGroupCache() {
	    return new DocumentationCache();
	  }

	  @Bean
	  public static ObjectMapperConfigurer objectMapperConfigurer() {
	    return new ObjectMapperConfigurer();
	  }

	  @Bean
	  public JsonSerializer jsonSerializer(List<JacksonModuleRegistrar> moduleRegistrars) {
	    return new JsonSerializer(moduleRegistrars);
	  }


	  @Bean
	  @Conditional(JadVanillaSpringMvcCondition.class)
	  public static PropertyPlaceholderConfigurer swaggerProperties() {
	    PropertyPlaceholderConfigurer properties = new PropertyPlaceholderConfigurer();
	    properties.setIgnoreUnresolvablePlaceholders(true);
	    return properties;
	  }
	
	
}

