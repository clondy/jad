/**
 * Copyright (c) 2011-2014, hubin (jobob@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.jad.commons.web.waf;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jad.commons.web.utils.RequestUtils;
import com.jad.commons.web.waf.request.WafRequestWrapper;

/**
 * Waf防火墙过滤器
 * <p>
 * @author   hubin
 * @Date	 2014-5-8 	 
 */
public class WafFilter implements Filter {

	private static final Logger logger = LoggerFactory.getLogger(WafFilter.class);

	private String overUrl = null;//非过滤地址

	private boolean filterXss = true;//开启XSS脚本过滤

	private boolean filterSql = true;//开启SQL注入过滤


	public void init( FilterConfig config ) throws ServletException {
		//读取Web.xml配置地址
		overUrl = config.getInitParameter("over.url");

		filterXss = getParamConfig(config.getInitParameter("filter_xss"));
		filterSql = getParamConfig(config.getInitParameter("filter_sql_injection"));
	}


	public void doFilter( ServletRequest request, ServletResponse response, FilterChain chain ) throws IOException,
		ServletException {
		HttpServletRequest req = (HttpServletRequest) request;

		boolean isOver = RequestUtils.inContainURL(req, overUrl);

		/** 非拦截URL、直接通过. */
		if(isOver){
			chain.doFilter(request, response);
		}
		
		//Request请求XSS过滤
		chain.doFilter(new WafRequestWrapper(req, filterXss, filterSql), response);
		
	}


	public void destroy() {
		logger.info(" WafFilter destroy .");
	}


	/**
	 * @Description 获取参数配置
	 * @param value
	 *            配置参数
	 * @return 未配置返回 True
	 */
	private boolean getParamConfig( String value ) {
		if ( value == null || "".equals(value.trim()) ) {
			//未配置默认 True
			return true;
		}
		return new Boolean(value);
	}
}
