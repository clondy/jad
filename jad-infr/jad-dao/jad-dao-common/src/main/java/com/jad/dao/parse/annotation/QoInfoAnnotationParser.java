package com.jad.dao.parse.annotation;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.jad.commons.vo.QueryObject;
import com.jad.dao.entity.QoFieldInfo;
import com.jad.dao.entity.QoMetaInfo;
import com.jad.dao.parse.QoInfoParser;
import com.jad.dao.utils.DaoReflectionUtil;

public class QoInfoAnnotationParser implements QoInfoParser{
	
	
	private CurdParser curdParser=new CurdParser();
	
	@Override
	public <QO extends QueryObject> QoMetaInfo<QO> parseQo(Class<QO> clazz) {
		
		QoMetaInfo<QO> qoInfo=new QoMetaInfo<QO>(clazz);
		
		qoInfo.setFieldInfoMap(getQoFieldInfos(clazz, qoInfo));
		
		return qoInfo;
	}
	
	
	/**
	 * 获取QO类所有字段
	 * 
	 * @param clazz
	 * @param eoInfo
	 */
	private <QO extends QueryObject> Map<String,QoFieldInfo<QO>> getQoFieldInfos(Class<QO> clazz, QoMetaInfo<QO> voInfo) {
		
		Map<String,QoFieldInfo<QO>> fieldInfoMap= new TreeMap<String,QoFieldInfo<QO>>();
		
		Set<Field> fields = DaoReflectionUtil.getClassFields(clazz);//获得所有属性
		
		for (Field field : fields) {
			QoFieldInfo<QO> qoFieldInfo = new QoFieldInfo<QO>(voInfo,field);
			
			curdParser.parseQoField(qoFieldInfo, field);
			
			if(qoFieldInfo.getCurd()!=null 
					&& qoFieldInfo.getCurd().queryConf()!=null){
				
				boolean isQuery =  qoFieldInfo.getCurd().queryConf().value();
				if(isQuery){
					fieldInfoMap.put(field.getName(), qoFieldInfo);
				}
			}
			//是否乎略此属性
//			boolean isIgore = qoFieldInfo.getQueryProperty()==null ? false : qoFieldInfo.getQueryProperty().isIgore();
//			if(!isIgore){
//				fieldInfoMap.put(field.getName(), qoFieldInfo);
//			}
		}
		return fieldInfoMap;
	}
	
	

}
