/**
 * Copyright (c) 2011-2014, hubin (jobob@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jad.dao.enums;

import javax.persistence.GenerationType;

/**
 * <p>
 * 生成ID类型枚举类
 * </p>
 * 
 * @Date 2015-11-10
 */
public enum IdType {
	
	TABLE(0, "table方式(jpa推荐)"),//jad暂时没有实现 
	SEQUENCE(1, "序列"),//目前jad只在oracle中实现，对于mysql自动切换为uuid
	IDENTITY(2, "数据库ID自增"), //jad暂时没有实现 
	AUTO(3, "自动判断"),//jad暂时没有实现 
	UUID(4, "全局唯一ID")//目前默认的方式
	;
	

	/** 主键 */
	private final int key;

	/** 描述 */
	private final String desc;

	IdType(final int key, final String desc) {
		
		this.key = key;
		this.desc = desc;
	}

	/**
	 * <p>
	 * 主键策略
	 * </p>
	 * 
	 * @param idType
	 *            ID 策略类型
	 * @return
	 */
	public static IdType getIdType(int idType) {
		IdType[] its = IdType.values();
		for (IdType it : its) {
			if (it.getKey() == idType) {
				return it;
			}
		}
		return UUID;
	}

	public int getKey() {
		return this.key;
	}

	public String getDesc() {
		return this.desc;
	}

}
