package com.jad.dao.entity;

import java.lang.reflect.Field;

import com.jad.commons.annotation.CURD;
import com.jad.commons.annotation.QueryConf;
import com.jad.commons.vo.QueryObject;

public class QoFieldInfo <QO extends QueryObject> extends ObjectFieldInfo{

	
	private QoMetaInfo<QO> qoInfo;
	
	/**
	 * 关联查询信息
	 */
	private CURD curd = null;
	
	public QoFieldInfo(QoMetaInfo<QO> qoInfo,Field field) {
		super(field);
		this.qoInfo=qoInfo;
	}

	public QoMetaInfo<QO> getQoInfo() {
		return qoInfo;
	}

	public void setQoInfo(QoMetaInfo<QO> qoInfo) {
		this.qoInfo = qoInfo;
	}

	public CURD getCurd() {
		return curd;
	}

	public void setCurd(CURD curd) {
		this.curd = curd;
	}




	
	


}
