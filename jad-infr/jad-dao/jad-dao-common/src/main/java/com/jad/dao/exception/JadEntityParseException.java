package com.jad.dao.exception;

public class JadEntityParseException extends RuntimeException{

	public JadEntityParseException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public JadEntityParseException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public JadEntityParseException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public JadEntityParseException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public JadEntityParseException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}


}
