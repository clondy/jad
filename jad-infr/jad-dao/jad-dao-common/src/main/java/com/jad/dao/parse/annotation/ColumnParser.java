package com.jad.dao.parse.annotation;

import java.lang.reflect.Field;

import javax.persistence.Column;

import com.jad.commons.utils.StringUtils;

import com.jad.commons.vo.EntityObject;
import com.jad.dao.entity.EoFieldInfo;

public class ColumnParser extends AbstractEoFieldAnnotationParser<Column>{
	
//	@Override
//	public boolean parseAnnotation(EoFieldInfo entityFieldInfo, Field field,Column annotation) {
//		
//		entityFieldInfo.setColumn(true);
//		
//		setColumnAttr(entityFieldInfo,field,annotation);
//		
//		return true;
//	}
	
	
	@Override
	public <EO extends EntityObject> boolean parseAnnotationEoField(
			EoFieldInfo<EO> eoFieldInfo, Field field, Column annotation) {
		eoFieldInfo.setColumn(true);
		
		setColumnAttrEo(eoFieldInfo,field,annotation);
		
		return true;
	}
	
	

	private static <EO extends EntityObject> void setColumnAttrEo(EoFieldInfo<EO> eoFieldInfo,Field field,Column column){
		
		String name = column.name();
		
		String fieldName=field.getName();
		
		eoFieldInfo.setFieldName(fieldName);
		
		eoFieldInfo.setColumn(StringUtils.isNotBlank(name) ? name  : fieldName);
		
		eoFieldInfo.setUnique(column.unique());
		
		eoFieldInfo.setNullable(column.nullable());
		
		eoFieldInfo.setInsertable(column.insertable());
		
		eoFieldInfo.setUpdatable(column.updatable());
		
		eoFieldInfo.setLength(column.length());
		
		eoFieldInfo.setPrecision(column.precision());
		
		eoFieldInfo.setScale(column.scale());
		
	}
	
	


}
