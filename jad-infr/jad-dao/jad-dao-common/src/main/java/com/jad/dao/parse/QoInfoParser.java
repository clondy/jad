package com.jad.dao.parse;

import com.jad.commons.vo.QueryObject;
import com.jad.dao.entity.QoMetaInfo;


/**
 * qo元信息解析
 * @author Administrator
 *
 */
public interface QoInfoParser {

	public <QO extends QueryObject> QoMetaInfo<QO> parseQo(Class<QO> clazz);
}
