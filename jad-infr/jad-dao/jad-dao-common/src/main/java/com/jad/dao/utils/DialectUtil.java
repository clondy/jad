package com.jad.dao.utils;

import com.jad.commons.context.Global;
import com.jad.dao.dialect.Dialect;
import com.jad.dao.dialect.db.DB2Dialect;
import com.jad.dao.dialect.db.DerbyDialect;
import com.jad.dao.dialect.db.H2Dialect;
import com.jad.dao.dialect.db.HSQLDialect;
import com.jad.dao.dialect.db.MySQLDialect;
import com.jad.dao.dialect.db.OracleDialect;
import com.jad.dao.dialect.db.PostgreSQLDialect;
import com.jad.dao.dialect.db.SQLServer2005Dialect;
import com.jad.dao.dialect.db.SybaseDialect;

public class DialectUtil {
	
	private static Dialect dialect;

	public static Dialect getDialect(){
		if(dialect==null){
			dialect=parseDialect();
		}
		return dialect;
	}
	
	
	private static Dialect parseDialect(){
		Dialect dialect = null;
        String dbType = Global.getDbType();
        if ("db2".equals(dbType)){
        	dialect = new DB2Dialect();
        }else if("derby".equals(dbType)){
        	dialect = new DerbyDialect();
        }else if("h2".equals(dbType)){
        	dialect = new H2Dialect();
        }else if("hsql".equals(dbType)){
        	dialect = new HSQLDialect();
        }else if("mysql".equals(dbType)){
        	dialect = new MySQLDialect();
        }else if("oracle".equals(dbType)){
        	dialect = new OracleDialect();
        }else if("postgre".equals(dbType)){
        	dialect = new PostgreSQLDialect();
        }else if("mssql".equals(dbType) || "sqlserver".equals(dbType)){
        	dialect = new SQLServer2005Dialect();
        }else if("sybase".equals(dbType)){
        	dialect = new SybaseDialect();
        }
        if (dialect == null) {
            throw new RuntimeException("无法识别数据库别名,请指定正确的全局配置参数:jdbc.type");
        }
        return dialect;
	}
	

}
