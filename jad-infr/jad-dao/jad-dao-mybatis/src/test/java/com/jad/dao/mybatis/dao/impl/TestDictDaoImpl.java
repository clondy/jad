package com.jad.dao.mybatis.dao.impl;

import com.jad.dao.AbstractJadEntityDao;
import com.jad.dao.annotation.JadDao;
import com.jad.dao.mybatis.dao.TestDictDao;
import com.jad.dao.mybatis.eo.TestDictEo;


@JadDao("testDictDao")
public class TestDictDaoImpl extends AbstractJadEntityDao<TestDictEo,String> implements TestDictDao {


}
