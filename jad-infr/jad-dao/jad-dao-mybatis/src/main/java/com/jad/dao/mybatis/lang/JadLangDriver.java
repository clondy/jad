package com.jad.dao.mybatis.lang;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.binding.MapperMethod;
import org.apache.ibatis.executor.parameter.ParameterHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.scripting.LanguageDriver;
import org.apache.ibatis.scripting.xmltags.XMLLanguageDriver;

import com.jad.commons.reflection.ReflectionUtils;

/**
 * jad自定义解析，主要解析sql中问号占位符
 * 这个方法作废，因为如果开启session本地缓存时，占位符中的参数无法参与缓存key的生成
 */
@Deprecated
public class JadLangDriver extends XMLLanguageDriver implements LanguageDriver {

	
//	 [语] prefix; prefixion
	
	private static final String LIST_PARAM_PREFIX="jadlistparamprefix";
	
	private static final String LIST_PARAM_NAME="jadListParam";

	public ParameterHandler createParameterHandler(MappedStatement mappedStatement,
			Object parameterObject, BoundSql boundSql) {
		
		
		if (parameterObject == null || !(parameterObject instanceof MapperMethod.ParamMap)) {
			return super.createParameterHandler(mappedStatement, parameterObject, boundSql);
		}
		
		MapperMethod.ParamMap paramMap=(MapperMethod.ParamMap)parameterObject;
		
		if(!paramMap.containsKey(LIST_PARAM_NAME) || !(paramMap.get(LIST_PARAM_NAME) instanceof List)){
			return super.createParameterHandler(mappedStatement, parameterObject, boundSql);
		}
		
		List paramList=(List)paramMap.get(LIST_PARAM_NAME);
		if(paramList.isEmpty()){
			return super.createParameterHandler(mappedStatement, parameterObject, boundSql);
		}
		
		List<ParameterMapping>addParamMappingList=new ArrayList<ParameterMapping>();
		
		for(int i=0;i<paramList.size();i++){
			String key=LIST_PARAM_PREFIX+i;
			ParameterMapping pm=new ParameterMapping.Builder(mappedStatement.getConfiguration(),key,Object.class).build();
			addParamMappingList.add(pm);
			paramMap.put(key, paramList.get(i));
		}
		
		List<ParameterMapping>oldList=boundSql.getParameterMappings();
		
		List<ParameterMapping>newParamMappingList=new ArrayList<ParameterMapping>();
		
//		for(ParameterMapping oldPm:oldList){
//			newParamMappingList.add(oldPm);
//		}
		newParamMappingList.addAll(oldList);
		
		newParamMappingList.addAll(addParamMappingList);
		
		ReflectionUtils.setFieldValue(boundSql, "parameterObject", paramMap);
		ReflectionUtils.setFieldValue(boundSql, "parameterMappings", newParamMappingList);//parameterMappings 
		
		return super.createParameterHandler(mappedStatement, parameterObject, boundSql);
		
	}

}
