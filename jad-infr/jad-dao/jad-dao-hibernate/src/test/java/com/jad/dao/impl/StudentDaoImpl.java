package com.jad.dao.impl;

import com.jad.dao.AbstractJadEntityDao;
import com.jad.dao.StudentDao;
import com.jad.dao.annotation.JadDao;
import com.jad.entity.Student;

@JadDao("studentDao")
public class StudentDaoImpl extends AbstractJadEntityDao<Student, String> implements StudentDao{

}
