package com.jad.test;

import java.io.FileInputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.jad.commons.enums.QueryOperateType;
import com.jad.entity.TestEntityType;
import com.jad.service.TestEntityTypeService;

public class TestEntityTypeTest {
	
	int intType=16;
	
	TestEntityTypeService userService=null;
	
	@BeforeTest
	public void beforeTest(){
		ApplicationContext context = new ClassPathXmlApplicationContext("spring-hibernate.xml");
		userService=(TestEntityTypeService)context.getBean("testEntityTypeService");
		
//		String[]names=context.getBeanDefinitionNames();
//        for(String name:names){
//        	System.out.println("beanName:"+name);
//        }
        
	}
	
	@Test
	public void mainTest()throws Exception{
		findTest();
	}
	
//	@Test
    public void findTest() throws Exception {
		Map<String,Object>conditionParams=new HashMap<String,Object>();
		List<Integer>intTypeIn=new ArrayList<Integer>();
		intTypeIn.add(2);
		intTypeIn.add(5);
		intTypeIn.add(8);
		
//		conditionParams.put("intType", intType);
//		conditionParams.put("intTypeIn", intTypeIn);
		
		Integer[]paramArr=new Integer[]{2,5};
		
		conditionParams.put("intType"+QueryOperateType.between.getSuffix(), paramArr);
		
		List<TestEntityType>list=userService.findBy(conditionParams);
		
		System.out.println("size:"+list.size());
//		System.out.println(JsonMapper.toJsonString(list));
		
//		byte[]blob=list.get(0).getBlob1();
//		if(blob!=null){
//			String file="E:\\tempfile\\testdb\\mz_db2.jpg";
//			OutputStream out=new FileOutputStream(file);
//			out.write(blob);
//			out.close();
//		}
		
		
	}
	
	
//	@Test
	public void updateTest() throws Exception {
		Map<String,Object>conditionParams=new HashMap<String,Object>();
		conditionParams.put("intType", intType);
		
		List<TestEntityType>list=userService.findBy(conditionParams);
		
		TestEntityType entity=list.get(0);
		
		Date d=entity.getUtilDate();
		Calendar c=Calendar.getInstance();
		c.setTime(d);
		c.add(Calendar.DAY_OF_YEAR, 1);
		
		entity.setUtilDate(c.getTime());
		
		entity.setClob1("更新鸟");
		
		FileInputStream fis = null;
		 try {
			fis = new FileInputStream("E:\\tempfile\\testdb\\mz2.jpg");  
			byte[] buffer = new byte[fis.available()];  
			fis.read(buffer);  
			fis.close();
//			byte[]blob1="hahahahahaha哈哈".getBytes();
			entity.setBlob1(buffer);
			 
		} finally {
			fis.close();
		}  
		
		
//		userService.updateBy(entity, conditionParams);
		
	}
	
	
	
	
	

//	@Test
    public void addTest() throws Exception {
		
		
		TestEntityType entity=new TestEntityType();
//		entity.setId(IdGen.uuid());
		
		
		
//		entity.setClob1("这是一本书和详细描述。#（*&#@￥%（*&@￥）（@#￥#￥中吕嗯啊");
		
		
//		E:\\tempfile\\testdb\\mz.jpg
		
		FileInputStream fis = null;
		 try {
			fis = new FileInputStream("E:\\tempfile\\testdb\\mz.jpg");  
			byte[] buffer = new byte[fis.available()];  
			fis.read(buffer);  
			fis.close();
//			byte[]blob1="hahahahahaha哈哈".getBytes();
//			entity.setBlob1(buffer);
			 
		} finally {
			fis.close();
		}  
     
		
		
		
		
		entity.setBigInteger(new BigInteger("5"));
		
		BigDecimal bd=new BigDecimal(3.14159);
		bd.setScale(3,BigDecimal.ROUND_HALF_UP);
		entity.setBigDecimal(bd);
		
//		InputStream in = new FileInputStream("F:\\4563123.jpg");
//        Blob blob = Hibernate.createBlob(in);
//        //得到简介的clob
//        Clob clob = Hibernate.createClob("这是一本书和详细描述。#（*&#@￥%（*&@￥）（@#￥#￥");
	        
		
//		@Column(name="util_date_type")
//		private java.util.Date utilDate;
		entity.setUtilDate(new java.util.Date());
//		
//		@Column(name="calendar_type")
//		private java.util.Calendar calendar;
		entity.setCalendar(Calendar.getInstance());
//		
//		@Column(name="sql_date_type")
//		private java.sql.Date sqlDate;
		entity.setSqlDate(new java.sql.Date(new java.util.Date().getTime()));
//		
//		@Column(name="sql_time_type")
//		private java.sql.Time sqlTime;
		entity.setSqlTime(new java.sql.Time(new java.util.Date().getTime()));
//		
//		@Column(name="timestamp_type")
//		private java.sql.Timestamp timestamp;
		entity.setTimestamp(new java.sql.Timestamp(new java.util.Date().getTime()));
		
		entity.setBooleanType(true);
		entity.setBooleanType2(false);
		
		entity.setCharType('d');
		entity.setCharType2('f');
		
		entity.setByteType((byte)12);
		entity.setByteType2((byte)13);
		
		entity.setShortType((short)14);
		entity.setShortType2((short)15);
		
		entity.setIntType(intType);
		entity.setIntType2(17);
		
		entity.setLongType(18l);
		entity.setLongType2(19l);
		
		entity.setFloatType(20.2f);
		entity.setFloatType2(24.5f);
		
		entity.setDoubleType(30.2);
		entity.setDoubleType2(34.2);
		
		userService.add(entity);
		
		
	}
	

}
