package com.jad.rpc.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.jad.commons.utils.StringUtils;
import org.springframework.web.context.WebApplicationContext;

import com.alibaba.dubbo.common.Constants;
import com.alibaba.dubbo.common.extension.ExtensionLoader;
import com.alibaba.dubbo.common.utils.ConfigUtils;
import com.alibaba.dubbo.container.Container;

public class DubboContextListener extends org.springframework.web.context.ContextLoaderListener {

	private static Logger logger = LoggerFactory.getLogger(DubboContextListener.class);

	public static final String CONTAINER_KEY = "dubbo.container";

	private static final ExtensionLoader<Container> loader = ExtensionLoader.getExtensionLoader(Container.class);

	public static final String SHUTDOWN_HOOK_KEY = "dubbo.shutdown.hook";

	@Override
	public WebApplicationContext initWebApplicationContext(ServletContext servletContext) {

		WebApplicationContext wc = super.initWebApplicationContext(servletContext);

		initContainer();

		System.out.println("初始化dubbo容器完成");

		return wc;

	}

	private static void initContainer() {

		String config = ConfigUtils.getProperty(CONTAINER_KEY,
				loader.getDefaultExtensionName());
		String[] args = Constants.COMMA_SPLIT_PATTERN.split(config);
		final List<Container> containers = new ArrayList<Container>();
		for (int i = 0; i < args.length; i++) {
			if (StringUtils.isNotBlank(args[i])) {
				containers.add(loader.getExtension(args[i]));
			}

		}
		logger.info("Use container type(" + Arrays.toString(args)
				+ ") to run dubbo serivce.");

		if ("true".equals(System.getProperty(SHUTDOWN_HOOK_KEY))) {
			Runtime.getRuntime().addShutdownHook(new Thread() {
				public void run() {
					for (Container container : containers) {
						try {
							container.stop();
							logger.info("Dubbo "+ container.getClass().getSimpleName()+ " stopped!");
						} catch (Throwable t) {
							logger.error(t.getMessage(), t);
						}
					}
				}
			});
		}

		for (Container container : containers) {
			logger.info("开始初始化容器:" + container.getClass().getSimpleName());
			container.start();
			logger.info("Dubbo " + container.getClass().getSimpleName()+ " started!");
		}
		System.out.println(new SimpleDateFormat("[yyyy-MM-dd HH:mm:ss]").format(new Date()) + " Dubbo service server started!");

	}

}
