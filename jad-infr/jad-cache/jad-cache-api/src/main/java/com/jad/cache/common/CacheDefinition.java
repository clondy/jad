package com.jad.cache.common;


/**
 * 
 * JadCache实例中的属性
 * 主要是为了活动方便的配置
 * @author Administrator
 * 
 */
public class CacheDefinition {
	
	private int defActivityTime;
	
	private boolean allowNullValues;
	
	public int getDefActivityTime() {
		return defActivityTime;
	}
	public void setDefActivityTime(int defActivityTime) {
		this.defActivityTime = defActivityTime;
	}
	public boolean isAllowNullValues() {
		return allowNullValues;
	}
	public void setAllowNullValues(boolean allowNullValues) {
		this.allowNullValues = allowNullValues;
	}
	
	

}
