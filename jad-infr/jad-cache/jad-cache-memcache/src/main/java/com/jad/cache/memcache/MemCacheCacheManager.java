package com.jad.cache.memcache;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.util.Assert;

import com.alisoft.xplatform.asf.cache.ICache;
import com.alisoft.xplatform.asf.cache.ICacheManager;
import com.alisoft.xplatform.asf.cache.memcached.MemcachedCacheManager;
import com.jad.cache.common.JadAbstractCacheManager;
import com.jad.cache.common.JadCache;

/**
 * memcache实现的CacheManager
 *
 */
public class MemCacheCacheManager extends JadAbstractCacheManager  {

	private static final Logger logger = LoggerFactory.getLogger(MemCacheCacheManager.class);
	
	private ICacheManager cacheManager;
	
	@Override
	protected Collection<? extends Cache> loadCaches() {
		
		Assert.notNull(this.getCacheClient(),"没有注入缓存客户端");
		Assert.notNull(cacheManager,"没有注入缓存管理器");
		
		Collection<Cache> caches = new LinkedHashSet<Cache>();
		
		if(cacheManager instanceof MemcachedCacheManager){
			
			//memcache客户端这时候还没有启动,暂时不加载memcache.xml中的cache。这要求管理客户端bean配置中需要配置memcache.xml中的cache
//			MemcachedCacheManager manager=(MemcachedCacheManager)cacheManager;
//			for(Map.Entry<String,IMemcachedCache>ent:manager.getCachepool().entrySet()){
//				caches.add(new MemCacheCache(cacheClient,ent.getKey(),ent.getValue(),activityTime,allowNullValues ));
//			}
			
		}else if (cacheManager instanceof LocalCacheManager){
			
			LocalCacheManager manager = (LocalCacheManager)cacheManager;
			
			if(manager.isMultiplexMasterCache()){
				
				caches.add(this.newCache(manager.getMasterCacheName()));
				
			}else{
				
				for(Map.Entry<String, ICache>ent:manager.getCacheMap().entrySet()){
					caches.add(this.newCache(ent.getKey()));
				}
				
			}
		}
		
		return caches;
	}
	
	
	protected Cache getMissingCache(String name) {
		
		Cache cache = super.getMissingCache(name);
		
		if(cache == null){
			
			Assert.notNull(cacheManager,"没有注入缓存管理器,"+this.getLoggerInfo(name));
			Assert.notNull(getCacheClient(),"没有注入缓存客户端,"+this.getLoggerInfo(name));
			
			if(!getCacheClient().isAutoCreateCache()){
				logger.warn("无法创建名称为:"+name+"的缓存，因为当前缓存管理器不允许自动创建缓存,clientName"+getCacheClient().getClientName());
				return null;
			}
			
			logger.debug("当前缓存中管理器中没有找到缓存，自动生成一个,"+this.getLoggerInfo(name));
			
			if(cacheManager instanceof MemcachedCacheManager){
				
				return newCache(name);
				
			}else if (cacheManager instanceof LocalCacheManager){
				
				LocalCacheManager manager = (LocalCacheManager)cacheManager;
				manager.addCache(name);
				
				return newCache(name);
			}
			
		}
		return cache;
	}
	
	
	
	public Cache newCache(JadCache jc){
		Assert.notNull(jc);
		Assert.notNull(this.getCacheClient(),"创建缓存失败,没有注入缓存客户端,"+this.getLoggerInfo(jc.getName()));
		
		int activityTime = jc.getDefActivityTime();
		boolean allowNullValues = jc.isAllowNullValues();
		
		return newCache(jc.getName(),activityTime,allowNullValues);
	}
	
	private Cache newCache(String name,int activityTime,boolean allowNullValues ) {
		
		if(cacheManager instanceof MemcachedCacheManager){
			
			return new MemCacheCache(this.getCacheClient(),name,(MemcachedCacheManager)cacheManager,activityTime,allowNullValues);
			
		}else if (cacheManager instanceof LocalCacheManager){
			
			return new MemCacheCache(this.getCacheClient(),name,(LocalCacheManager)cacheManager,activityTime,allowNullValues);
		}
		return null;
	}
	
	public Cache newCache(String name) {
		
		Assert.notNull(this.getCacheClient(),"创建缓存失败,没有注入缓存客户端,"+this.getLoggerInfo(name));
		
		int activityTime = this.getCacheClient().getDefActivityTime() == null ? -1 : this.getCacheClient().getDefActivityTime().intValue();
		boolean allowNullValues = this.getCacheClient().getAllowNullValues() == null ? false : this.getCacheClient().getAllowNullValues().booleanValue();
		
		return newCache(name,activityTime,allowNullValues);
		
	}
	
	
	public void start() {
		this.getCacheManager().start();
	}

	public void stop() {
		this.getCacheManager().stop();
	}

	public ICacheManager getCacheManager() {
		return cacheManager;
	}

	public void setCacheManager(ICacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	








}
