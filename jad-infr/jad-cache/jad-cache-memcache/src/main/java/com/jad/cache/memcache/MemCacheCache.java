package com.jad.cache.memcache;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.alisoft.xplatform.asf.cache.ICache;
import com.alisoft.xplatform.asf.cache.ICacheManager;
import com.alisoft.xplatform.asf.cache.impl.DefaultCacheImpl;
import com.alisoft.xplatform.asf.cache.memcached.MemcachedCache;
import com.jad.cache.common.AbstractManageredCache;
import com.jad.cache.common.CacheClient;

/**
 * memcache实现的cache
 *
 */
public class MemCacheCache extends AbstractManageredCache{
	
	private static final Logger logger = LoggerFactory.getLogger(MemCacheCache.class);
	
	private final ICacheManager cacheManager;
	
	private final String name;
	
	public MemCacheCache(CacheClient cacheClient,String name,ICacheManager cacheManager,int activityTime,boolean allowNullValues) {
		super(cacheClient,activityTime,allowNullValues);
		Assert.notNull(name, "name不能为null");
		Assert.notNull(cacheManager, "cacheManager不能为null");
		this.name = name;
		this.cacheManager = cacheManager;
	}
	
	@Override
	public String getName() {
		return name;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public ICache getNativeCache() {
		return cacheManager.getCache(name);
	}
	
	@Override
	protected Object lookup(Object key) {
		String keyStr=wrapperKey(key);
		
		if(!isClientStarted()){
			logger.debug("缓存还没有启动，无法从缓存中获得数据,"+this.getLoggerInfo(key==null?null:key.toString()));
			return null;
		}
		
//		logger.debug("缓存总数:"+this.getCacheClient().size(this.name)+","+this.getLoggerInfo(name, null));
		
		return this.getNativeCache().get(keyStr);
	}
	
	/**
	 * 包装一下key
	 * 在 key前面加上 name前缀
	 * @param key
	 * @return
	 */
	protected String wrapperKey(Object key){
		return name+"_"+key;
	}
	@Override
	public void put(Object key, Object value) {
		
		put(key,value,this.getActivityTime());
	}
	
	@Override
	public void put(Object key, Object value,int activityTime) {
		
		if(key==null){
			logger.warn("key为null,不能缓存对像,"+this.getLoggerInfo(null));
			return ;
		}
		if(!isClientStarted()){
			logger.debug("缓存还没有启动，无法把数据保存到缓存中,"+this.getLoggerInfo(key.toString()));
			return ;
		}
		
		if(!super.isAllowNullValues() && value == null ){
			logger.warn("不能把null对像整到缓存中,"+getLoggerInfo(key.toString()));
			return ;
		}
		
		logger.debug("准备缓存对像,有效秒数:"+activityTime+","+this.getLoggerInfo(key.toString()));
		
		String keyStr=wrapperKey(key);
		this.getNativeCache().put(keyStr, toStoreValue(value,activityTime));
	}

	@Override
	public ValueWrapper putIfAbsent(Object key, Object value) {
		
		if(key==null){
			logger.warn("key为null,不能缓存对像,"+this.getLoggerInfo(null));
			return null;
		}
		
		if(!isClientStarted()){
			logger.debug("缓存还没有启动，无法把数据保存到缓存中,"+this.getLoggerInfo(key.toString()));
			return null;
		}
		
		if(!super.isAllowNullValues() && value == null ){
			logger.warn("不能把null对像整到缓存中,"+getLoggerInfo(key.toString()));
			return null;
		}
		
		logger.debug("准备缓存对像,有效秒数:"+this.getActivityTime()+","+this.getLoggerInfo(key.toString()));
		String keyStr=wrapperKey(key);
		Object obj = this.getNativeCache().put(keyStr, toStoreValue(value,this.getActivityTime()));
		return toValueWrapper(obj,key);
		
	}
	
	

	@Override
	public void evict(Object key) {
		
		if(!isClientStarted()){
			logger.debug("缓存还没有启动，无法把数据从缓存中删除,"+this.getLoggerInfo(key==null?null:key.toString()));
			return ;
		}
		String keyStr=wrapperKey(key);
		logger.debug("准备清除对像,"+this.getLoggerInfo(key.toString()));
		this.getNativeCache().remove(keyStr);
	}

	@Override
	public void clear() {
		if(!isClientStarted()){
			logger.debug("缓存还没有启动，无法清空缓存,"+this.getLoggerInfo(null));
			return ;
		}
		logger.debug("准备清空对像,"+this.getLoggerInfo(null));
		this.getNativeCache().clear();
	}
	
	@Override
	public Integer size() {
		if(!isClientStarted()){
			logger.debug("缓存还没有启动，法统计缓存总数,"+this.getLoggerInfo(null));
			return 0;
		}
		int size = keySet().size();
		logger.debug("统计出对像总数:"+size+","+this.getLoggerInfo(null));
		return size;
	}
	
	@Override
	public Set<String> keySet() {
		Set<String> set=new HashSet<String>();
		if(!isClientStarted()){
			logger.debug("缓存还没有启动，无法获得缓存keyset,"+this.getLoggerInfo(null));
			return set;
		}
		ICache cache = getNativeCache();
		if(cache instanceof DefaultCacheImpl){
			set = ((DefaultCacheImpl)cache).keySet();
		}else if(cache instanceof MemcachedCache){
			set = ((MemcachedCache)cache).keySet(true);//快速遍历,不准确
		}
		
		Set<String> resSet=new HashSet<String>();
		for(String key:set){
			if(key.startsWith(name)){
				resSet.add(key);
			}
		}
		return resSet;
	}
	
	
	
	
	
	
	

	

}
