package com.jad.cache.ehcache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class EhCacheTest  {

	private static Logger logger=LoggerFactory.getLogger(EhCacheTest.class); 
	
	@Test
    public void testCache() throws Exception {
		
		AccountService accountService = (AccountService)context.getBean("accountService");
		logger.debug("调用第一次。。。");
		accountService.getAccountByName("testUserName");
	
//		EhcacheClientManager manager = (EhcacheClientManager)context.getBean("cacheClientManager");
//		manager.stop();
		
//		((EhcacheClient)manager.getCacheClient()).getCacheManager().shutdown();
		
		logger.debug("调用第二次。。。");
		accountService.getAccountByName("testUserName");
		
//		AccountCao cao = (AccountCao)context.getBean("accountCao");
		
//		logger.debug("调用第三次。。。");
//		accountService.getAccountByName2("testUserName");
		
//		logger.debug("调用第4次。。。");
//		Account act=accountService.getAccountByName2("testUserName");
		
//		logger.debug("调用第5次。。。");
//		Account2 act2=accountService.getAccountByName3("testUserName");
		
		
//		Object obj = cao.getFromCache("testUserName");
		
//		logger.debug("测试通过。。。:"+(obj==null));
		
		logger.debug("测试通过。。。:");
	}
	
	protected ApplicationContext context ;
	
	@BeforeTest
    public void baseBeforeTest() throws Exception {
		logger.debug("context 正在初始化...");
		context = new ClassPathXmlApplicationContext(getContextConfigFile());
		logger.debug("context 初始化完成...");
	}
	
	public String getContextConfigFile() {
		return "spring-cache-ehcache.xml";
	}
	
	
	
}
