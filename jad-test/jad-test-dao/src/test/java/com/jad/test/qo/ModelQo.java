package com.jad.test.qo;

import com.jad.commons.vo.QueryObject;

public class ModelQo implements QueryObject{
	
	private String id;
	
	private Integer intType;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Integer getIntType() {
		return intType;
	}
	public void setIntType(Integer intType) {
		this.intType = intType;
	}
	
	
}
