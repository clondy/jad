package com.jad.test.dao;

import com.jad.dao.JadEntityDao;
import com.jad.dao.annotation.JadDao;
import com.jad.test.entity.ModelEo;


@JadDao
public interface ModelDao extends JadEntityDao<ModelEo,String> {


}
