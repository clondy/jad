package com.jad.commons.enums;

/**
 * 控件类型
 * @author Administrator
 *
 */
public enum WidgetType {
	
	DEFAULT(0, "默认"), 
	STR_INPUT(1, "文本框"), DATE_INPUT(2, "日期选择");
	
	private final int type;

	private final String desc;

	WidgetType(final int type, final String desc) {
		this.type = type;
		this.desc = desc;
	}
	
}



