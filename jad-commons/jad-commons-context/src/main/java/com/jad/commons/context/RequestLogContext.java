package com.jad.commons.context;

import java.io.Serializable;
import java.util.Map;

import org.springframework.core.NamedThreadLocal;


/**
 * @author Administrator
 *
 */
public class RequestLogContext implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final ThreadLocal<RequestLogContext> THREAD_LOCALREQUEST_CONTEXT = new NamedThreadLocal<RequestLogContext>("ThreadLocal RequestContext");
	
	private String remoteAddr; 	// 请求的IP地址
	private String requestUri; 	// 请求的URI
	private String method; 		// 请求方式
	private Map params; 		// 请求提交的数据
	private String userAgent;	// 请求用户代理信息
	private String title;//请求名称
	
	public static RequestLogContext getCurrRequestContext(){
		return THREAD_LOCALREQUEST_CONTEXT.get();
	}
	
	public static void putRequestContext(RequestLogContext context){
		THREAD_LOCALREQUEST_CONTEXT.set(context);
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getRemoteAddr() {
		return remoteAddr;
	}
	public void setRemoteAddr(String remoteAddr) {
		this.remoteAddr = remoteAddr;
	}
	public String getRequestUri() {
		return requestUri;
	}
	public void setRequestUri(String requestUri) {
		this.requestUri = requestUri;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getUserAgent() {
		return userAgent;
	}
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}
	public Map getParams() {
		return params;
	}
	public void setParams(Map params) {
		this.params = params;
	}

}
