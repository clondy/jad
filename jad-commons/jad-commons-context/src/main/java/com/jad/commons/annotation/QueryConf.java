package com.jad.commons.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.jad.commons.enums.DataType;
import com.jad.commons.enums.QueryOperateType;
import com.jad.commons.enums.WidgetType;


@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface QueryConf {
	
	boolean value() default true;
	
	String label() default "";
	
	DataType dataType() default DataType.DEFAULT;
	
	WidgetType widgetType() default WidgetType.DEFAULT;
	
	/**
	 * 关联属性
	 * 如果是关联属性，以“.”号分隔
	 * @return
	 */
	String property() default "";
	
	/**
	 * 查询操作类型
	 * @return
	 */
	QueryOperateType queryOperateType() default QueryOperateType.eq;
	
}







