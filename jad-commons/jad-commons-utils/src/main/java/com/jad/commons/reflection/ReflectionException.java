package com.jad.commons.reflection;

public class ReflectionException extends RuntimeException {

	public ReflectionException() {
		// TODO Auto-generated constructor stub
	}

	public ReflectionException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ReflectionException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ReflectionException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ReflectionException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
