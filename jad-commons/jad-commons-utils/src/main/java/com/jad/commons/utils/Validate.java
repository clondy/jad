package com.jad.commons.utils;

import java.util.Collection;

public class Validate extends org.apache.commons.lang3.Validate{
	
	
	public static void notBlank(String str) {
        if (StringUtils.isBlank(str)) {
            throw new IllegalArgumentException("str param can not blank");
        }
    }
	
	public static void notBlank(String str, String message) {
        if (StringUtils.isBlank(str)) {
            throw new IllegalArgumentException(message);
        }
    }
	
	public static void notEmptyList(final Collection<?> coll){
		notEmptyList(coll,null);
	}
	
	public static void notEmptyList(final Collection<?> coll, final String message){
		if(coll == null || CollectionUtils.isEmpty(coll)){
			String msg = message == null ? "集合列表不能为空" : message;
			throw new IllegalStateException(msg);
		}
	}
	
	public static void validState(final boolean expression, final String message, final Object... values) {
        if (expression == false) {
            throw new IllegalStateException(String.format(message, values));
        }
    }
	
	public static void validState(final boolean expression, final String message) {
        if (expression == false) {
            throw new IllegalStateException(message);
        }
    }
	
	public static void allNotNull(Object...objs){
		notEmpty(objs);
		for(Object obj:objs){
			notNull(obj);
		}
	}
	
	public static void allNotBlank(String...strs){
		notEmpty(strs);
		for(String str:strs){
			notBlank(str);
		}
	}
	
	
	

}
