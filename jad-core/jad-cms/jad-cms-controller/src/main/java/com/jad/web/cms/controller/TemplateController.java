package com.jad.web.cms.controller;

import javax.servlet.ServletContext;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jad.core.cms.service.SiteService;
import com.jad.core.cms.utils.FileTplUtil;
import com.jad.core.cms.vo.SiteVo;
import com.jad.web.cms.util.SiteHelper;
import com.jad.web.mvc.BaseController;

/**
 * 站点Controller
 * @author SongLai
 * @version 2013-3-23
 */
@Controller
@RequestMapping(value = "${adminPath}/cms/template")
public class TemplateController extends BaseController {

    @Autowired
   	private SiteService siteService;
    
    @Autowired
    private ServletContext context;

    @RequiresPermissions("cms:template:edit")
   	@RequestMapping(value = "")
   	public String index() {
   		return "modules/cms/tplIndex";
   	}

    @RequiresPermissions("cms:template:edit")
   	@RequestMapping(value = "tree")
   	public String tree(Model model) {
        SiteVo site = siteService.findById(SiteHelper.getCurrentSiteId());
        
        String path=site.getSolutionPath();
        
   		model.addAttribute("templateList", FileTplUtil.getListForEdit(context.getRealPath(path),context.getRealPath("")));
   		return "modules/cms/tplTree";
   	}

    @RequiresPermissions("cms:template:edit")
   	@RequestMapping(value = "form")
   	public String form(String name, Model model) {
        model.addAttribute("template", FileTplUtil.getFileTpl(name));
   		return "modules/cms/tplForm";
   	}

    @RequiresPermissions("cms:template:edit")
   	@RequestMapping(value = "help")
   	public String help() {
   		return "modules/cms/tplHelp";
   	}
}
