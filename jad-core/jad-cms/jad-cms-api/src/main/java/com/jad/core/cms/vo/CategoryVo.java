/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.cms.vo;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jad.commons.annotation.CURD;
import com.jad.commons.vo.TreeVo;
import com.jad.core.cms.utils.ArticleUtil;
import com.jad.core.cms.utils.CategoryUtil;
import com.jad.core.sys.vo.OfficeVo;

/**
 * 栏目Entity
 * @author ThinkGem
 * @version 2013-05-15
 */
public class CategoryVo extends TreeVo<CategoryVo,String> {

    public static final String DEFAULT_TEMPLATE = "frontList";

	private static final long serialVersionUID = 1L;
	
	private CategoryVo parent;
	
	private SiteVo site;		// 归属站点
	private OfficeVo office;//归属部门
	
	@CURD(label="栏目模型")
	private String module; 	// 栏目模型（article：文章；picture：图片；download：下载；link：链接；special：专题）
	
	@CURD(label="栏目图片")
	private String image; 	// 栏目图片
	
	@CURD(label="链接")
	private String href; 	// 链接
	
	@CURD(label="目标")
	private String target; 	// 目标（ _blank、_self、_parent、_top）
	
	@CURD(label="描述")
	private String description; 	// 描述，填写有助于搜索引擎优化
	
	@CURD(label="关键字")
	private String keywords; 	// 关键字，填写有助于搜索引擎优化
	
	@CURD(label="是否在导航中显示")
	private String inMenu; 		// 是否在导航中显示（1：显示；0：不显示）
	
	@CURD(label="是否在分类页中显示")
	private String inList; 		// 是否在分类页中显示列表（1：显示；0：不显示）
	
	@CURD(label="展现方式")
	private String showModes; 	// 展现方式（0:有子栏目显示栏目列表，无子栏目显示内容列表;1：首栏目内容列表；2：栏目第一条内容）
	
	@CURD(label="是否允许评论")
	private String allowComment;// 是否允许评论
	
	@CURD(label="是否需要审核")
	private String isAudit;	// 是否需要审核
	
	@CURD(label="自定义列表视图")
	private String customListView;		// 自定义列表视图
	
	@CURD(label="自定义内容视图")
	private String customContentView;	// 自定义内容视图
	
	@CURD(label="视图参数")
    private String viewConfig;	// 视图参数
	
	@CURD(label="信息量")
    private String cnt;//信息量
	
	@CURD(label="点击量")
    private String hits;//点击量
    
	private String url;
    
	
//	private List<CategoryVo> childList = Lists.newArrayList(); 	// 拥有子分类列表

	public CategoryVo(){
		super();
//		this.module = "";
//		this.sort = 30;
//		this.inMenu = Constants.HIDE;
//		this.inList = Constants.SHOW;
//		this.showModes = "0";
//		this.allowComment = Constants.NO;
//		this.delFlag = Constants.DEL_FLAG_NORMAL;
//		this.isAudit = Constants.NO;
	}

	public CategoryVo(String id){
		this();
		this.id = id;
	}
	
	

  	

	@JsonIgnore
	public OfficeVo getOffice() {
		return office;
	}

	public void setOffice(OfficeVo office) {
		this.office = office;
	}

	@JsonIgnore
	public SiteVo getSite() {
		return site;
	}

	public String getHits() {
		return hits;
	}

	public void setHits(String hits) {
		this.hits = hits;
	}

	public void setSite(SiteVo site) {
		this.site = site;
	}
	
//	public OfficeVo getOffice() {
//		return office;
//	}
//
//	public void setOffice(OfficeVo office) {
//		this.office = office;
//	}
	

	@JsonBackReference
	public CategoryVo getParent() {
		return parent;
	}

	public void setParent(CategoryVo parent) {
		this.parent = parent;
	}
	
//	@Length(min=1, max=255)
//	public String getParentIds() {
//		return parentIds;
//	}
//
//	public void setParentIds(String parentIds) {
//		this.parentIds = parentIds;
//	}
	
	@Length(min=0, max=20)
	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

//	@Length(min=0, max=100)
//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}

	@Length(min=0, max=255)
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Length(min=0, max=255)
	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	@Length(min=0, max=20)
	public String getTarget() {
		return target;
	}



	public void setTarget(String target) {
		this.target = target;
	}


	@Length(min=0, max=255)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Length(min=0, max=255)
	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	
//	@NotNull
//	public Integer getSort() {
//		return sort;
//	}
//
//	public void setSort(Integer sort) {
//		this.sort = sort;
//	}

	@Length(min=1, max=1)
	public String getInMenu() {
		return inMenu;
	}

	public void setInMenu(String inMenu) {
		this.inMenu = inMenu;
	}

	@Length(min=1, max=1)
	public String getInList() {
		return inList;
	}

	public void setInList(String inList) {
		this.inList = inList;
	}

	@Length(min=1, max=1)
	public String getShowModes() {
		return showModes;
	}

	public void setShowModes(String showModes) {
		this.showModes = showModes;
	}
	
	@Length(min=1, max=1)
	public String getAllowComment() {
		return allowComment;
	}

	public void setAllowComment(String allowComment) {
		this.allowComment = allowComment;
	}

	@Length(min=1, max=1)
	public String getIsAudit() {
		return isAudit;
	}

	public void setIsAudit(String isAudit) {
		this.isAudit = isAudit;
	}

	public String getCustomListView() {
		return customListView;
	}

	public void setCustomListView(String customListView) {
		this.customListView = customListView;
	}

	public String getCustomContentView() {
		return customContentView;
	}

	public void setCustomContentView(String customContentView) {
		this.customContentView = customContentView;
	}

    public String getViewConfig() {
        return viewConfig;
    }

    public void setViewConfig(String viewConfig) {
        this.viewConfig = viewConfig;
    }

//	public List<CategoryVo> getChildList() {
//		return childList;
//	}
//
//	public void setChildList(List<CategoryVo> childList) {
//		this.childList = childList;
//	}

	public String getCnt() {
		return cnt;
	}

	public void setCnt(String cnt) {
		this.cnt = cnt;
	}
	
//	public static void sortList(List<CategoryVo> list, List<CategoryVo> sourcelist, String parentId){
//		for (int i=0; i<sourcelist.size(); i++){
//			CategoryVo e = sourcelist.get(i);
//			if (e.getParent()!=null && e.getParent().getId()!=null
//					&& e.getParent().getId().equals(parentId)){
//				list.add(e);
//				// 判断是否还有子节点, 有则继续获取子节点
//				for (int j=0; j<sourcelist.size(); j++){
//					CategoryVo child = sourcelist.get(j);
//					if (child.getParent()!=null && child.getParent().getId()!=null
//							&& child.getParent().getId().equals(e.getId())){
//						sortList(list, sourcelist, e.getId());
//						break;
//					}
//				}
//			}
//		}
//	}
	
	public String getIds() {
		return (this.getParentIds() !=null ? this.getParentIds().replaceAll(",", " ") : "") 
				+ (this.getId() != null ? this.getId() : "");
	}

	public boolean isRoot(){
		return CategoryUtil.isRoot(this.id);
	}
	

  	public String getUrl() {
        return url;
   	}
  	
	public void setUrl(String url) {
		this.url=url;
	}
	
//   	public String getUrl(String contextPath,String frontPath,String urlSuffix) {
//        return CategoryUtil.getUrlDynamic(this,contextPath,frontPath,urlSuffix);
//   	}

 
    
}