/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.cms.vo;

import org.hibernate.validator.constraints.Length;

import com.jad.commons.annotation.CURD;
import com.jad.commons.vo.BaseVo;
import com.jad.core.cms.constant.SiteConstant;

/**
 * 站点Entity
 * @author ThinkGem
 * @version 2013-05-15
 */
public class SiteVo extends BaseVo<String>  {
	
	private static final long serialVersionUID = 1L;
	
	@CURD(label="站点名称")
	private String name;	// 站点名称
	
	@CURD(label="站点标题")
	private String title;	// 站点标题
	
	@CURD(label="站点logo")
	private String logo;	// 站点logo
	
	@CURD(label="描述")
	private String description;// 描述，填写有助于搜索引擎优化
	
	@CURD(label="关键字")
	private String keywords;// 关键字，填写有助于搜索引擎优化
	
	@CURD(label="主题")
	private String theme;	// 主题
	
	@CURD(label="版权信息")
	private String copyright;// 版权信息
	
	@CURD(label="自定义首页视图文件")
	private String customIndexView;// 自定义首页视图文件
	
	@CURD(label="域名")
	private String domain;

	public SiteVo() {
		super();
	}
	
	public SiteVo(String id){
		this();
		this.id = id;
	}

	@Length(min=1, max=100)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Length(min=1, max=100)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	@Length(min=0, max=255)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Length(min=0, max=255)
	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	@Length(min=1, max=255)
	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public String getCopyright() {
		return copyright;
	}

	public void setCopyright(String copyright) {
		this.copyright = copyright;
	}

	public String getCustomIndexView() {
		return customIndexView;
	}

	public void setCustomIndexView(String customIndexView) {
		this.customIndexView = customIndexView;
	}

    /**
   	 * 获得模板方案路径。如：/WEB-INF/views/modules/cms/front/themes/jeesite
   	 *
   	 * @return
   	 */
   	public String getSolutionPath() {
   		return SiteConstant.TPL_BASE + "/" + getTheme();
   	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}
	
}