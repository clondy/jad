package com.jad.core.cms.qo;

import com.jad.commons.annotation.CURD;
import com.jad.commons.annotation.QueryConf;
import com.jad.commons.qo.BaseQo;

public class CommentQo extends BaseQo<String>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@CURD(label="分类编号",queryConf=@QueryConf(property="category.id"))
	private String categoryId;// 分类编号
	
	@CURD(label="编号")
	private String contentId;	// 归属分类内容的编号（Article.id、Photo.id、Download.id）
	
	
	@CURD(label="标题")
	private String title;	// 归属分类内容的标题（Article.title、Photo.title、Download.title）
	
	@CURD(label="评论内容")
	private String content; // 评论内容
	
	public CommentQo() {
	}
	public CommentQo(String id) {
		super(id);
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getContentId() {
		return contentId;
	}
	public void setContentId(String contentId) {
		this.contentId = contentId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}

}
