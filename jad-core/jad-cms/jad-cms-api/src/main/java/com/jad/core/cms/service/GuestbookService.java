/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.cms.service;

import com.jad.commons.service.CurdService;
import com.jad.commons.vo.Page;
import com.jad.core.cms.vo.GuestbookVo;

/**
 * 留言Service
 * @author ThinkGem
 * @version 2013-01-15
 */
public interface GuestbookService extends CurdService<GuestbookVo,String> {

//	public void delete(GuestbookVo guestbook, Boolean isRe) ;
	
	/**
	 * 更新索引
	 */
//	public void createIndex();
	
	/**
	 * 全文检索
	 */
	//FIXME 暂不提供
//	public Page<GuestbookVo> search(Page<GuestbookVo> page, String q, String beginDate, String endDate);
	
}
