package com.jad.core.cms.dao.impl;

import com.jad.core.cms.dao.SiteDao;
import com.jad.core.cms.entity.SiteEo;
import com.jad.dao.AbstractJadEntityDao;
import com.jad.dao.annotation.JadDao;

@JadDao("siteDao")
public class SiteDaoImpl extends AbstractJadEntityDao<SiteEo,String>implements SiteDao{


}
