/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.cms.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jad.commons.service.impl.AbstractServiceImpl;
import com.jad.core.cms.entity.LinkEo;
import com.jad.core.cms.service.LinkService;
import com.jad.core.cms.vo.LinkVo;

/**
 * 链接Service
 * @author ThinkGem
 * @version 2013-01-15
 */
@Service("linkService")
@Transactional(readOnly = true)
public class LinkServiceImpl extends AbstractServiceImpl<LinkEo, String,LinkVo> implements LinkService {

	
//	Date updateExpiredWeightDate =new Date();
	
//	@Autowired
//	private LinkDao linkDao;
	
//	@Transactional(readOnly = false)
//	public Page<LinkVo> findPage(Page<LinkVo> page, LinkQo link, boolean isDataScopeFilter) {
//		// 更新过期的权重，间隔为“6”个小时
////		Date updateExpiredWeightDate =  (Date)CacheUtils.get("updateExpiredWeightDateByLink");
//		if (updateExpiredWeightDate == null || (updateExpiredWeightDate != null 
//				&& updateExpiredWeightDate.getTime() < new Date().getTime())){
//			linkDao.updateExpiredWeight();
////			CacheUtils.put("updateExpiredWeightDateByLink", DateUtils.addHours(new Date(), 6));
//			updateExpiredWeightDate=DateUtils.addHours(new Date(), 6);
//		}
//		
////		link.getSqlMap().put("dsf", dataScopeFilter(link.getCurrentUser(), "o", "u"));
//		
////		UserVo user=UserUtils.getUser();
////		link.getSqlMap().put("dsf", DataFilters.dataScopeFilter(user, "o", "u"));
//		
//		return super.findPage(page, link);
//	}
	
//	@Transactional(readOnly = false)
//	public void delete(LinkVo link, Boolean isRe) {
//		//dao.updateDelFlag(id, isRe!=null&&isRe?Link.DEL_FLAG_NORMAL:Link.DEL_FLAG_DELETE);
//		link.setDelFlag(isRe!=null&&isRe?Constants.DEL_FLAG_NORMAL:Constants.DEL_FLAG_DELETE);
////		dao.delete(link);
//		linkDao.deleteById(link.getId());
//	}
	
	/**
	 * 通过编号获取内容标题
	 */
//	public List<Object[]> findByIds(String ids) {
//		List<Object[]> list = Lists.newArrayList();
//		String[] idss = StringUtils.split(ids,",");
//		if (idss.length>0){
////			List<LinkEo> l = dao.findByIdIn(idss);
////			List<LinkEo> l =linkDao.findByIdIn(idss);
//			List<LinkEo> l = linkDao.findByIdList(Arrays.asList(idss));
//			
//			for (LinkEo e : l){
//				list.add(new Object[]{e.getId(),StringUtils.abbr(e.getTitle(),50)});
//			}
//		}
//		return list;
//	}

}
