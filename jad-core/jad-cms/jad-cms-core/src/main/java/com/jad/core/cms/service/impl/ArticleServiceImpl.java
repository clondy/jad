/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.cms.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jad.commons.service.impl.AbstractServiceImpl;
import com.jad.core.cms.dao.ArticleDao;
import com.jad.core.cms.entity.ArticleEo;
import com.jad.core.cms.service.ArticleDataService;
import com.jad.core.cms.service.ArticleService;
import com.jad.core.cms.vo.ArticleVo;

/**
 * 文章Service
 * @author ThinkGem
 * @version 2013-05-15
 */
@Service("articleService")
@Transactional(readOnly = true)
public class ArticleServiceImpl extends AbstractServiceImpl<ArticleEo, String,ArticleVo > implements ArticleService {

	@Autowired
	private ArticleDao articleDao;
	@Autowired
	private ArticleDataService articleDataService;
	
	/**
	 * 更新
	 * @param vo
	 */
	@Transactional(readOnly = false)
	public void update(ArticleVo articleVo){
		super.update(articleVo);
		if(articleVo.getArticleData()!=null){
			articleVo.getArticleData().setId(articleVo.getId());
			articleDataService.update(articleVo.getArticleData());
		}
	}
	
	@Override
	@Transactional(readOnly = false)
	public void add(ArticleVo articleVo) {
		super.add(articleVo);
		if(articleVo.getArticleData()!=null){
			articleDataService.add(articleVo.getArticleData());
			articleDataService.updateId(articleVo.getId(), articleVo.getArticleData().getId());
			
		}
	}
	
	/**
	 * 删除数据
	 * @param entity
	 */
	public void delete(ArticleVo articleVo){
		super.delete(articleVo);
		if(articleVo.getArticleData()!=null && StringUtils.isNotBlank(articleVo.getArticleData().getId())){
			articleDataService.delete(articleVo.getArticleData());
		}
	}
	
	
//	private Date updateExpiredWeightDate=new Date();//待完善
	
//	@Transactional(readOnly = false)
//	public Page<ArticleVo> findPage(Page<ArticleVo> page, ArticleQo articleQo, boolean isDataScopeFilter) {
//		// 更新过期的权重，间隔为“6”个小时
////		Date updateExpiredWeightDate =  (Date)CacheUtils.get("updateExpiredWeightDateByArticle");
////		if (updateExpiredWeightDate == null || (updateExpiredWeightDate != null 
////				&& updateExpiredWeightDate.getTime() < new Date().getTime())){
////			articleDao.updateExpiredWeight();
//////			CacheUtils.put("updateExpiredWeightDateByArticle", DateUtils.addHours(new Date(), 6));
////			updateExpiredWeightDate=DateUtils.addHours(new Date(), 6);
////		}
////		DetachedCriteria dc = dao.createDetachedCriteria();
////		dc.createAlias("category", "category");
////		dc.createAlias("category.site", "category.site");
//		
//		CategoryQo cqo=new CategoryQo();
//		
//		if (StringUtils.isNotBlank(articleQo.getCategoryId()) && !CategoryUtil.isRoot(articleQo.getCategoryId())){
//			
////			CategoryVo category = categoryDao.get(article.getCategory().getId());
//			CategoryVo category = EntityUtils.copyToVo(categoryDao.findById(articleQo.getCategoryId())
//						, CategoryVo.class);
//			
//			if (category==null){
//				category = new CategoryVo();
//			}
//			
//			
//			
//			cqo.setParentIds(category.getId());
//			cqo.setSiteId(category.getSite().getId());
////			cqo.setCategory(category);
//		}
//		else{
////			article.setCategory(new CategoryVo());
//		}
////		if (StringUtils.isBlank(page.getOrderBy())){
////			page.setOrderBy("a.weight,a.update_date desc");
////		}
////		return dao.find(page, dc);
//	//	article.getSqlMap().put("dsf", dataScopeFilter(article.getCurrentUser(), "o", "u"));
//		return super.findPage(page, cqo);
//		
//	}

//	@Transactional(readOnly = false)
//	public void save(ArticleVo article) {
//		
//		if (article.getArticleData().getContent()!=null){
//			article.getArticleData().setContent(StringEscapeUtils.unescapeHtml4(
//					article.getArticleData().getContent()));
//		}
//		// 如果没有审核权限，则将当前内容改为待审核状态
////		if (!UserUtils.getSubject().isPermitted("cms:article:audit")){
////			article.setDelFlag(Constants.DEL_FLAG_AUDIT);
////		}
//		// 如果栏目不需要审核，则将该内容设为发布状态
//		if (article.getCategory()!=null&&StringUtils.isNotBlank(article.getCategory().getId())){
////			CategoryVo category = categoryDao.get(article.getCategory().getId());
//			CategoryVo category = EntityUtils.copyToVo(categoryDao.findById(article.getCategory().getId())
//					, CategoryVo.class);
//		
//			
//			if (!Constants.YES.equals(category.getIsAudit())){
//				article.setDelFlag(Constants.DEL_FLAG_NORMAL);
//			}
//		}
////		article.setUpdateBy(UserUtils.getUser());
//		article.setUpdateDate(new Date());
//        if (StringUtils.isNotBlank(article.getViewConfig())){
//            article.setViewConfig(StringEscapeUtils.unescapeHtml4(article.getViewConfig()));
//        }
//        
//        ArticleDataVo articleData = new ArticleDataVo();
//		if (StringUtils.isBlank(article.getId())){
////			article.preInsert();
//			super.preInsert(article,null);
//			
//			articleData = article.getArticleData();
//			articleData.setId(article.getId());
//			
////			dao.insert(article);
////			articleDataDao.insert(articleData);
//			
//			articleDao.add(EntityUtils.copyToEo(article, ArticleEo.class));
//			articleDataDao.add(EntityUtils.copyToEo(articleData, ArticleDataEo.class));
//			
//		}else{
////			article.preUpdate();
//			super.preUpdate(article);
//			
//			articleData = article.getArticleData();
//			articleData.setId(article.getId());
//			
////			dao.update(article);
////			articleDataDao.update(article.getArticleData());
//			articleDao.updateById(EntityUtils.copyToEo(article, ArticleEo.class),article.getId());
//			articleDataDao.updateById(EntityUtils.copyToEo(articleData, ArticleDataEo.class),article.getId());
//			
//		}
//	}
	
//	@Transactional(readOnly = false)
//	public void delete(ArticleVo article, Boolean isRe) {
////		dao.updateDelFlag(id, isRe!=null&&isRe?Article.DEL_FLAG_NORMAL:Article.DEL_FLAG_DELETE);
//		// 使用下面方法，以便更新索引。
//		//Article article = dao.get(id);
//		//article.setDelFlag(isRe!=null&&isRe?Article.DEL_FLAG_NORMAL:Article.DEL_FLAG_DELETE);
//		//dao.insert(article);
//		super.delete(article);
//	}
	
	/**
	 * 通过编号获取内容标题
	 * @return new Object[]{栏目Id,文章Id,文章标题}
	 */
//	public List<Object[]> findByIds(String ids) {
//		if(ids == null){
//			return new ArrayList<Object[]>();
//		}
//		List<Object[]> list = Lists.newArrayList();
//		String[] idss = StringUtils.split(ids,",");
//		for(int i=0;(idss.length-i)>0;i++){
////			e = dao.get(idss[i]);
//			ArticleEo e=articleDao.findById(idss[i]);
//			list.add(new Object[]{e.getCategory().getId(),e.getId(),StringUtils.abbr(e.getTitle(),50)});
//		}
//		return list;
//	}
	
	/**
	 * 点击数加一
	 */
	@Transactional(readOnly = false)
	public void updateHitsAddOne(String id) {
		articleDao.updateHitsAddOne(id);
	}
	
//	/**
//	 * 更新索引
//	 */
//	public void createIndex(){
//		//dao.createIndex();
//	}
	
//	/**
//	 * 全文检索
//	 */
//	//FIXME 暂不提供检索功能
//	public Page<ArticleVo> search(Page<ArticleVo> page, String q, String categoryId, String beginDate, String endDate){
//		
//		// 设置查询条件
////		BooleanQuery query = dao.getFullTextQuery(q, "title","keywords","description","articleData.content");
////		
////		// 设置过滤条件
////		List<BooleanClause> bcList = Lists.newArrayList();
////
////		bcList.add(new BooleanClause(new TermQuery(new Term(Article.FIELD_DEL_FLAG, Article.DEL_FLAG_NORMAL)), Occur.MUST));
////		if (StringUtils.isNotBlank(categoryId)){
////			bcList.add(new BooleanClause(new TermQuery(new Term("category.ids", categoryId)), Occur.MUST));
////		}
////		
////		if (StringUtils.isNotBlank(beginDate) && StringUtils.isNotBlank(endDate)) {   
////			bcList.add(new BooleanClause(new TermRangeQuery("updateDate", beginDate.replaceAll("-", ""),
////					endDate.replaceAll("-", ""), true, true), Occur.MUST));
////		}   
//		
//		//BooleanQuery queryFilter = dao.getFullTextQuery((BooleanClause[])bcList.toArray(new BooleanClause[bcList.size()]));
//
////		System.out.println(queryFilter);
//		
//		// 设置排序（默认相识度排序）
//		//FIXME 暂时不提供lucene检索
//		//Sort sort = null;//new Sort(new SortField("updateDate", SortField.DOC, true));
//		// 全文检索
//		//dao.search(page, query, queryFilter, sort);
//		// 关键字高亮
//		//dao.keywordsHighlight(query, page.getList(), 30, "title");
//		//dao.keywordsHighlight(query, page.getList(), 130, "description","articleData.content");
//		
//		return page;
//	}
	
}
