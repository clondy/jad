package com.jad.core.cms.dao.impl;

import com.jad.core.cms.dao.ArticleDataDao;
import com.jad.core.cms.entity.ArticleDataEo;
import com.jad.dao.AbstractJadEntityDao;
import com.jad.dao.annotation.JadDao;

@JadDao("articleDataDao")
public class ArticleDataDaoImpl extends AbstractJadEntityDao<ArticleDataEo,String>implements ArticleDataDao{


}
