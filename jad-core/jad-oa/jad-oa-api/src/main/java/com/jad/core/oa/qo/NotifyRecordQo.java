package com.jad.core.oa.qo;

import com.jad.commons.annotation.CURD;
import com.jad.commons.annotation.QueryConf;
import com.jad.commons.qo.BaseQo;

public class NotifyRecordQo extends BaseQo<String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@CURD(label="消息id",queryConf=@QueryConf(property="notify.id"))
	private String notifyId;
	
	private String readFlag;		// 阅读标记（0：未读；1：已读）
	
	private String userId;//接收人

	
	
	public NotifyRecordQo() {
	}
	
	public NotifyRecordQo(String id) {
		super(id);
	}
	
	

	public String getReadFlag() {
		return readFlag;
	}

	public void setReadFlag(String readFlag) {
		this.readFlag = readFlag;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getNotifyId() {
		return notifyId;
	}

	public void setNotifyId(String notifyId) {
		this.notifyId = notifyId;
	}

	
	
}
