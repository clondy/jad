package com.jad.core.oa.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.jad.commons.vo.Page;
import com.jad.commons.vo.PageQo;
import com.jad.core.oa.dao.NotifyDao;
import com.jad.core.oa.entity.NotifyEo;
import com.jad.core.oa.qo.NotifyQo;
import com.jad.dao.AbstractJadEntityDao;
import com.jad.dao.annotation.JadDao;
import com.jad.dao.entity.EoMetaInfo;
import com.jad.dao.utils.EntityUtils;
import com.jad.dao.utils.SqlHelper;

@JadDao("notifyDao")
public class NotifyDaoImpl extends AbstractJadEntityDao<NotifyEo,String>implements NotifyDao{

	@Override
	public Page<NotifyEo> findUserNodify(PageQo page, NotifyQo notifyQo,String userId){
		
		List<Object> params=new ArrayList();
		
		EoMetaInfo<NotifyEo> ei = EntityUtils.getEoInfo(NotifyEo.class);
		
		String alias = SqlHelper.getDefAlias(ei);
		
		StringBuffer sb=new StringBuffer();
		
		sb.append("select ");
		sb.append(SqlHelper.getSelectColumn(ei, alias));
		sb.append(" from oa_notify ").append(alias);
		
		if(StringUtils.isNotBlank(userId)){
			sb.append(" join oa_notify_record r on r.notify_id = ").append(alias).append(".id ");
		}
		sb.append(SqlHelper.getWhereSql(ei, alias, notifyQo, null, params));
		if(StringUtils.isNotBlank(userId)){
			sb.append(" and r.user_id = ? ");
			params.add(userId);
		}
		
		return findPageBySql(page, sb.toString(), params);
	}
	
	
	@Override
	public Long findUserNodifyCount(NotifyQo notifyQo ,String userId) {
		
		List<Object> params=new ArrayList();
		
		EoMetaInfo<NotifyEo> ei = EntityUtils.getEoInfo(NotifyEo.class);
		
		String alias = "a" ;
		
		StringBuffer sb=new StringBuffer();
		sb.append("select count(*) C from  oa_notify a ").append(alias);
		
		if(StringUtils.isNotBlank(userId)){
			sb.append(" join oa_notify_record r on r.notify_id = a.id ");
		}
		
		sb.append(SqlHelper.getWhereSql(ei, alias, notifyQo, null, params));
		
		if(StringUtils.isNotBlank(userId)){
			sb.append(" and r.user_id = ? ");
			params.add(userId);
		}
		
		return super.findCountBySql(sb.toString(), params);
	}



}



