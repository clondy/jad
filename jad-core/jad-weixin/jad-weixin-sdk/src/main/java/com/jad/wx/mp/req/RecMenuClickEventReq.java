package com.jad.wx.mp.req;

/**
 * 点击菜单拉取消息时的事件推送
 * @author hechuan
 *
 */
public class RecMenuClickEventReq extends RecEventMsgReq {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private String eventKey ;//	事件KEY值，与自定义菜单接口中KEY值对应  

	public String getEventKey() {
		return eventKey;
	}

	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}

}
