package com.jad.wx.mp.resp;

/**
 * 文本消息回复
 * @author hechuan
 *
 */
public class TextMsgResp  extends MsgReplyResp{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String content 	;//是 	回复的消息内容（换行：在content中能够换行，微信客户端就支持换行显示） 

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
