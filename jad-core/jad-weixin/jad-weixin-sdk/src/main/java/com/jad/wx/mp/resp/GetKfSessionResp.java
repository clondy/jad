package com.jad.wx.mp.resp;

public class GetKfSessionResp extends CommonResp{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String kf_account;//	正在接待的客服，为空表示没有人在接待
	private String createtime;//	会话接入的时间
	
	public String getKf_account() {
		return kf_account;
	}
	public void setKf_account(String kf_account) {
		this.kf_account = kf_account;
	}
	public String getCreatetime() {
		return createtime;
	}
	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}

}
