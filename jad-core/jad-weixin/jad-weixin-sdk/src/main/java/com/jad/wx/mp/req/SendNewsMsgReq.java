package com.jad.wx.mp.req;


/**
 * 发送图文消息
 * @author hechuan
 *
 */
public class SendNewsMsgReq extends SendMsgReq{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private SendNewsContentMsgReq news;
	public SendNewsContentMsgReq getNews() {
		return news;
	}
	public void setNews(SendNewsContentMsgReq news) {
		this.news = news;
	}

}
