package com.jad.wx.mp.service;

import com.jad.wx.mp.constant.WeixinMpException;
import com.jad.wx.mp.req.CreateMenuReq;
import com.jad.wx.mp.resp.CommonResp;
import com.jad.wx.mp.resp.GetMenuConfResp;
import com.jad.wx.mp.resp.GetMenuResp;

/**
 * 自定义菜单
 * @author hechuan
 *
 */
public interface MenuService {
	
	/**
	 * 创建自定义菜单
	 * http请求方式：POST（请使用https协议） 
	 	https://api.weixin.qq.com/cgi-bin/menu/create?access_token=ACCESS_TOKEN
	 * @param req
	 * @param access_token
	 * @return
	 * @throws WeixinMpException
	 */
	CommonResp createMenu(CreateMenuReq req,String access_token)throws WeixinMpException;	
	

	/**
	 * 查询自定义菜单
	 * http请求方式：GET
		https://api.weixin.qq.com/cgi-bin/menu/get?access_token=ACCESS_TOKEN
	 * @param access_token
	 * @return
	 * @throws WeixinMpException
	 */
	GetMenuResp getMenu(String access_token)throws WeixinMpException;
	
	

	/**
	 * 删除自定义菜单
	 * 使用接口创建自定义菜单后，开发者还可使用接口删除当前使用的自定义菜单。
	 * 另请注意，在个性化菜单时，调用此接口会删除默认菜单及全部个性化菜单。
		http请求方式：GET
		https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=ACCESS_TOKEN
	 * @param access_token
	 * @return
	 * @throws WeixinMpException
	 */
	CommonResp delMenu(String access_token)throws WeixinMpException;
	
	
	/**
	 * 获取自定义菜单配置接口
	 * 
	 * 本接口将会提供公众号当前使用的自定义菜单的配置，如果公众号是通过API调用设置的菜单，则返回菜单的开发配置，
	 * 而如果公众号是在公众平台官网通过网站功能发布菜单，则本接口返回运营者设置的菜单配置。
	 * 
	 * 1、第三方平台开发者可以通过本接口，在旗下公众号将业务授权给你后，立即通过本接口检测公众号的自定义菜单配置，
	 	并通过接口再次给公众号设置好自动回复规则，以提升公众号运营者的业务体验。
		2、本接口与自定义菜单查询接口的不同之处在于，本接口无论公众号的接口是如何设置的，都能查询到接口，
			而自定义菜单查询接口则仅能查询到使用API设置的菜单配置。
		3、认证/未认证的服务号/订阅号，以及接口测试号，均拥有该接口权限。
		4、从第三方平台的公众号登录授权机制上来说，该接口从属于消息与菜单权限集。
		5、本接口中返回的mediaID均为临时素材（通过素材管理-获取临时素材接口来获取这些素材），每次接口调用返回的mediaID都是临时的、不同的，
		在每次接口调用后3天有效，若需永久使用该素材，需使用素材管理接口中的永久素材。
	 * @param access_token
	 * @return
	 * @throws WeixinMpException
	 */
	GetMenuConfResp getMenuConf(String access_token)throws WeixinMpException;
	

}
