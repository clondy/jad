package com.jad.wx.mp.req;

import java.io.Serializable;

/**
 * 获得永久素材列表
 * @author hechuan
 *
 */
public class GetMaterialListReq implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public String type ;//	是 	素材的类型，图片（image）、视频（video）、语音 （voice）、图文（news）
	public String offset;// 	是 	从全部素材的该偏移位置开始返回，0表示从第一个素材 返回
	public String count 	;//是 	返回素材的数量，取值在1到20之间 
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getOffset() {
		return offset;
	}
	public void setOffset(String offset) {
		this.offset = offset;
	}
	public String getCount() {
		return count;
	}
	public void setCount(String count) {
		this.count = count;
	}

}
