package com.jad.wx.mp.resp;

import java.util.List;

/**
 * 获得永久素材响应
 * @author hechuan
 *
 */
public class GetAttachmentResp extends CommonResp {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<GetAttachmentItemResp>news_item;

	public List<GetAttachmentItemResp> getNews_item() {
		return news_item;
	}

	public void setNews_item(List<GetAttachmentItemResp> news_item) {
		this.news_item = news_item;
	}
	

}
