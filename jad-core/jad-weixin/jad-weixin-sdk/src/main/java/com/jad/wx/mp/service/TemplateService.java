package com.jad.wx.mp.service;

import com.jad.wx.mp.constant.WeixinMpException;
import com.jad.wx.mp.req.RecTemplatesendjobfinishEventReq;
import com.jad.wx.mp.req.TemplateMsgReq;
import com.jad.wx.mp.resp.CommonResp;
import com.jad.wx.mp.resp.GetIndustryInfoResp;
import com.jad.wx.mp.resp.GetTemplateIdResp;
import com.jad.wx.mp.resp.GetTemplateListResp;
import com.jad.wx.mp.resp.TemplateMsgResp;

/**
 * 模板消息服务
 * 
 * 1、所有服务号都可以在功能->添加功能插件处看到申请模板消息功能的入口，但只有认证后的服务号才可以申请模板消息的使用权限并获得该权限；
	2、需要选择公众账号服务所处的2个行业，每月可更改1次所选行业；
	3、在所选择行业的模板库中选用已有的模板进行调用；
	4、每个账号可以同时使用15个模板。
	5、当前每个账号的模板消息的日调用上限为10万次，单个模板没有特殊限制。
	【2014年11月18日将接口调用频率从默认的日1万次提升为日10万次，可在MP登录后的开发者中心查看】。
	当账号粉丝数超过10W/100W/1000W时，模板消息的日调用上限会相应提升，以公众号MP后台开发者中心页面中标明的数字为准。
	
	1、模板消息调用时主要需要模板ID和模板中各参数的赋值内容；
	2、模板中参数内容必须以".DATA"结尾，否则视为保留字；
	3、模板保留符号"{{ }}

 * @author hechuan
 *
 */
public interface TemplateService {
	
	/**
	 * 设置所属行业 
	 * 每月可修改行业1次，账号仅可使用所属行业中相关的模板
	 * http请求方式: POST
		https://api.weixin.qq.com/cgi-bin/template/api_set_industry?access_token=ACCESS_TOKEN
	 * @param industry_id1  是 	公众号模板消息所属行业编号 
	 * @param industry_id2  是 	公众号模板消息所属行业编号 
	 * @param access_token
	 */
	CommonResp setIndustry(String industry_id1,String industry_id2,String access_token)throws WeixinMpException;

	/**
	 * 获取设置的行业信息
	 * http请求方式：GET
		https://api.weixin.qq.com/cgi-bin/template/get_industry?access_token=ACCESS_TOKEN
	 * @param access_token
	 * @return
	 */
	public GetIndustryInfoResp getIndustryInfo(String access_token)throws WeixinMpException;
	
	/**
	 * 从行业模板库选择模板到账号后台
	 * http请求方式: POST
		https://api.weixin.qq.com/cgi-bin/template/api_add_template?access_token=ACCESS_TOKEN
	 * @param template_id_short 模板库中模板的编号，有“TM**”和“OPENTMTM**”等形式
	 * @param access_token
	 */
	GetTemplateIdResp getTemplate(String template_id_short,String access_token)throws WeixinMpException;
	
	/**
	 * 发送模板消息
	 * https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN
	 * @param req
	 * @return
	 * @throws WeixinMpException
	 */
	TemplateMsgResp sendTemplateMsg(TemplateMsgReq req,String access_token)throws WeixinMpException;
	 
	/**
	 * 获得模板列表
	 * @param access_token
	 * @return
	 * @throws WeixinMpException
	 */
	GetTemplateListResp getTemplateList(String access_token)throws WeixinMpException;
	
	/**
	 * 删除模板
	 * @param template_id
	 * @param access_token
	 * @return
	 * @throws WeixinMpException
	 */
	CommonResp delTemplateList(String template_id,String access_token)throws WeixinMpException;
	
	
	/**
	 * 在模版消息发送任务完成后，微信服务器会将是否送达成功作为通知，发送到开发者中心中填写的服务器配置地址中。 
	 * @param req
	 */
	 void dealRecTemplatesendjobfinishEventReq(RecTemplatesendjobfinishEventReq req)throws WeixinMpException;
	
}
