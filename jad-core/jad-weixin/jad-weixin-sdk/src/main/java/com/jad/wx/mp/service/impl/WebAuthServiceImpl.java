package com.jad.wx.mp.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jad.commons.encrypt.Encodes;
import com.jad.commons.http.HttpResp;
import com.jad.commons.http.HttpUtil;
import com.jad.commons.http.MyHttpException;
import com.jad.commons.json.JsonMapper;
import com.jad.wx.mp.constant.MpConstant;
import com.jad.wx.mp.constant.WeixinMpException;
import com.jad.wx.mp.resp.CommonResp;
import com.jad.wx.mp.resp.GetUserInfoResp;
import com.jad.wx.mp.resp.RefreshTokenResp;
import com.jad.wx.mp.resp.WebAuthorizeResp;
import com.jad.wx.mp.service.WebAuthService;

/**
 * 网页授权服务
 * @author hechuan
 *
 */
public class WebAuthServiceImpl extends AbstractWxServiceImpl implements WebAuthService {
	
	private static Logger logger=LoggerFactory.getLogger(WebAuthServiceImpl.class);
	
	public static void main(String[] args)throws Exception {
		
		String appid="wx40659e5cfc5bea25";
		String secret="5e7acbb8068b4a9b9dde6881311c64fe";
		String openid="o2zDCt7_YbDAUXBY-fINZ1W4VVkk";
		String access_token=MpConstant.TOKEN;
		
		CommonResp resp=null;
		WebAuthServiceImpl service=new WebAuthServiceImpl();
		
		String url=service.genAuthorizeUrl(appid, "", MpConstant.SNSAPI_BASE_SCOPE, "state");
		System.out.println(url);
		
	}
	
	public CommonResp checkToken(String openid, String access_token)throws WeixinMpException {
		// TODO Auto-generated method stub
//		https://api.weixin.qq.com/sns/auth?access_token=ACCESS_TOKEN&openid=OPENID 
		
		StringBuffer sb=new StringBuffer();
		sb.append("https://api.weixin.qq.com");
		
		sb.append("/sns/auth");
		sb.append("?access_token=").append(access_token);
		sb.append("&openid=").append(openid);
		
		String url=sb.toString();
		
		try {
			HttpResp httpResp=HttpUtil.get(url);
			
			CommonResp resp=(CommonResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), CommonResp.class);
			
			checkSuccess(resp);
			
			return resp;
			
		} catch (MyHttpException e) {
			
			logger.error("通讯异常,"+e.getMessage(),e);
			
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
			
		}
		
	}
	
	@Override
	public RefreshTokenResp refreshToken(String appid, String refresh_token)throws WeixinMpException {
		// TODO Auto-generated method stub
		
//		https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=APPID&grant_type=refresh_token&refresh_token=REFRESH_TOKEN
		
		StringBuffer sb=new StringBuffer();
		sb.append("https://api.weixin.qq.com");
		
		sb.append("/sns/oauth2/refresh_token");
		sb.append("?appid=").append(appid);
		sb.append("&grant_type=refresh_token");
		sb.append("&refresh_token=").append(refresh_token);
		
		String url=sb.toString();
		
		try {
			HttpResp httpResp=HttpUtil.get(url);
			
			RefreshTokenResp resp=(RefreshTokenResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), RefreshTokenResp.class);
			
			checkSuccess(resp);
			
			return resp;
			
		} catch (MyHttpException e) {
			
			logger.error("通讯异常,"+e.getMessage(),e);
			
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
			
		}
		
		
	}
	
	@Override
	public GetUserInfoResp getWxmpUserInfo( String openid,String access_token) throws WeixinMpException {

		// TODO Auto-generated method stub
//		 https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN 
		String url="https://api.weixin.qq.com/sns/userinfo?access_token="+access_token+"&openid="+openid+"&lang=zh_CN";
		
		try {
			HttpResp httpResp=HttpUtil.get(url);
			
			GetUserInfoResp resp=(GetUserInfoResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), GetUserInfoResp.class);
			
			checkSuccess(resp);
			
			return resp;
		} catch (MyHttpException e){
			logger.error("通讯异常,"+e.getMessage(),e);
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
		}
	}
	
	
	@Override
	public WebAuthorizeResp getWebAuthorize(String appid,String secret,String code )throws WeixinMpException{
		
		// TODO Auto-generated method stub
		
		StringBuffer sb=new StringBuffer();
		sb.append("https://api.weixin.qq.com");
		sb.append("/sns/oauth2/access_token");
		sb.append("?appid=").append(appid);
		sb.append("&secret=").append(secret);
		sb.append("&code=").append(code);
		sb.append("&grant_type=authorization_code");
		
//		https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code 
		
		String url=sb.toString();
		
		try {
			HttpResp httpResp=HttpUtil.get(url);
			
			WebAuthorizeResp resp=(WebAuthorizeResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), WebAuthorizeResp.class);
			
			checkSuccess(resp);
			
			return resp;
			
		} catch (MyHttpException e) {
			
			logger.error("通讯异常,"+e.getMessage(),e);
			
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
			
		}
	}

	
	@Override
	public String genAuthorizeUrl(String appid, String redirect_uri, String scope, String state)  {
		StringBuffer sb=new StringBuffer();
		sb.append("https://open.weixin.qq.com/connect/oauth2/authorize?appid=");
		sb.append(appid);
		sb.append("&redirect_uri=").append(Encodes.urlEncode(redirect_uri));
		sb.append("&response_type=code");
		sb.append("&scope=").append(scope);
		if(StringUtils.isNotBlank(state)){
			sb.append("&state=").append(state);
		}
		sb.append("#wechat_redirect");
		
		return sb.toString();
	
	}
	

}
