package com.jad.wx.mp.resp;

import java.util.List;

/**
 * 获取客服列表
 * @author hechuan
 *
 */
public class GetKfAccountListResp extends CommonResp {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<KfAccountInfo> kf_list;

	public List<KfAccountInfo> getKf_list() {
		return kf_list;
	}

	public void setKf_list(List<KfAccountInfo> kf_list) {
		this.kf_list = kf_list;
	}

}
