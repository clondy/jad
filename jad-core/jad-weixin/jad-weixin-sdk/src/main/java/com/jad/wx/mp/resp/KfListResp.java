package com.jad.wx.mp.resp;

import java.io.Serializable;
import java.util.List;

/**
 * 客服列表
 * @author hechuan
 *
 */
public class KfListResp implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<KfInfoResp>kf_list;

	public List<KfInfoResp> getKf_list() {
		return kf_list;
	}

	public void setKf_list(List<KfInfoResp> kf_list) {
		this.kf_list = kf_list;
	}
	

}
