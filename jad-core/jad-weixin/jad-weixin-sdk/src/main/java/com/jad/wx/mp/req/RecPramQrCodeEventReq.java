package com.jad.wx.mp.req;

/**
 * 扫描带参数的二维码
 * @author hechuan
 *
 */
public class RecPramQrCodeEventReq extends RecEventMsgReq {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private String eventKey 	;//事件KEY值，qrscene_为前缀，后面为二维码的参数值
	private String ticket ;//	二维码的ticket，可用来换取二维码图片 
	
	public String getEventKey() {
		return eventKey;
	}
	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}
	public String getTicket() {
		return ticket;
	}
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

}
