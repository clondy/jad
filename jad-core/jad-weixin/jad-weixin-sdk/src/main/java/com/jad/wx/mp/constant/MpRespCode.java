package com.jad.wx.mp.constant;

import java.util.HashMap;
import java.util.Map;

/**
 * 共众平台全局返回码
 * @author hechuan
 *
 */
public class MpRespCode {
	
	public static Map<String ,MpRespCode>ALL=new HashMap<String,MpRespCode>();
	
	public static MpRespCode CODE__1=new MpRespCode("-1","系统繁忙，此时请开发者稍候再试");
	public static MpRespCode CODE_0=new MpRespCode("0","请求成功");
	public static MpRespCode CODE_40001=new MpRespCode("40001","获取access_token时AppSecret错误，或者access_token无效。请开发者认真比对AppSecret的正确性，或查看是否正在为恰当的公众号调用接口");
	public static MpRespCode CODE_40002=new MpRespCode("40002","不合法的凭证类型");
	public static MpRespCode CODE_40003=new MpRespCode("40003","不合法的OpenID，请开发者确认OpenID（该用户）是否已关注公众号，或是否是其他公众号的OpenID");
	public static MpRespCode CODE_40004=new MpRespCode("40004","不合法的媒体文件类型");
	public static MpRespCode CODE_40005=new MpRespCode("40005","不合法的文件类型");
	public static MpRespCode CODE_40006=new MpRespCode("40006","不合法的文件大小");
	public static MpRespCode CODE_40007=new MpRespCode("40007","不合法的媒体文件id");
	public static MpRespCode CODE_40008=new MpRespCode("40008","不合法的消息类型");
	public static MpRespCode CODE_40009=new MpRespCode("40009","不合法的图片文件大小");
	public static MpRespCode CODE_40010=new MpRespCode("40010","不合法的语音文件大小");
	public static MpRespCode CODE_40011=new MpRespCode("40011","不合法的视频文件大小");
	public static MpRespCode CODE_40012=new MpRespCode("40012","不合法的缩略图文件大小");
	public static MpRespCode CODE_40013=new MpRespCode("40013","不合法的AppID，请开发者检查AppID的正确性，避免异常字符，注意大小写");
	public static MpRespCode CODE_40014=new MpRespCode("40014","不合法的access_token，请开发者认真比对access_token的有效性（如是否过期），或查看是否正在为恰当的公众号调用接口");
	public static MpRespCode CODE_40015=new MpRespCode("40015","不合法的菜单类型");
	public static MpRespCode CODE_40016=new MpRespCode("40016","不合法的按钮个数");
	public static MpRespCode CODE_40017=new MpRespCode("40017","不合法的按钮个数");
	public static MpRespCode CODE_40018=new MpRespCode("40018","不合法的按钮名字长度");
	public static MpRespCode CODE_40019=new MpRespCode("40019","不合法的按钮KEY长度");
	public static MpRespCode CODE_40020=new MpRespCode("40020","不合法的按钮URL长度");
	public static MpRespCode CODE_40021=new MpRespCode("40021","不合法的菜单版本号");
	public static MpRespCode CODE_40022=new MpRespCode("40022","不合法的子菜单级数");
	public static MpRespCode CODE_40023=new MpRespCode("40023","不合法的子菜单按钮个数");
	public static MpRespCode CODE_40024=new MpRespCode("40024","不合法的子菜单按钮类型");
	public static MpRespCode CODE_40025=new MpRespCode("40025","不合法的子菜单按钮名字长度");
	public static MpRespCode CODE_40026=new MpRespCode("40026","不合法的子菜单按钮KEY长度");
	public static MpRespCode CODE_40027=new MpRespCode("40027","不合法的子菜单按钮URL长度");
	public static MpRespCode CODE_40028=new MpRespCode("40028","不合法的自定义菜单使用用户");
	public static MpRespCode CODE_40029=new MpRespCode("40029","不合法的oauth_code");
	public static MpRespCode CODE_40030=new MpRespCode("40030","不合法的refresh_token");
	public static MpRespCode CODE_40031=new MpRespCode("40031","不合法的openid列表");
	public static MpRespCode CODE_40032=new MpRespCode("40032","不合法的openid列表长度");
	public static MpRespCode CODE_40033=new MpRespCode("40033","不合法的请求字符");//不合法的请求字符，不能包含xxxx格式的字符
	public static MpRespCode CODE_40035=new MpRespCode("40035","不合法的参数");
	public static MpRespCode CODE_40038=new MpRespCode("40038","不合法的请求格式");
	public static MpRespCode CODE_40039=new MpRespCode("40039","不合法的URL长度");
	public static MpRespCode CODE_40050=new MpRespCode("40050","不合法的分组id");
	public static MpRespCode CODE_40051=new MpRespCode("40051","分组名字不合法");
	public static MpRespCode CODE_40117=new MpRespCode("40117","分组名字不合法");
	public static MpRespCode CODE_40118=new MpRespCode("40118","media_id大小不合法");
	public static MpRespCode CODE_40119=new MpRespCode("40119","button类型错误");
	public static MpRespCode CODE_40120=new MpRespCode("40120","button类型错误");
	public static MpRespCode CODE_40121=new MpRespCode("40121","不合法的media_id类型");
	public static MpRespCode CODE_40132=new MpRespCode("40132","微信号不合法");
	public static MpRespCode CODE_40137=new MpRespCode("40137","不支持的图片格式");
	public static MpRespCode CODE_41001=new MpRespCode("41001","缺少access_token参数");
	public static MpRespCode CODE_41002=new MpRespCode("41002","缺少appid参数");
	public static MpRespCode CODE_41003=new MpRespCode("41003","缺少refresh_token参数");
	public static MpRespCode CODE_41004=new MpRespCode("41004","缺少secret参数");
	public static MpRespCode CODE_41005=new MpRespCode("41005","缺少多媒体文件数据");
	public static MpRespCode CODE_41006=new MpRespCode("41006","缺少media_id参数");
	public static MpRespCode CODE_41007=new MpRespCode("41007","缺少子菜单数据");
	public static MpRespCode CODE_41008=new MpRespCode("41008","缺少oauth code");
	public static MpRespCode CODE_41009=new MpRespCode("41009","缺少openid");
	public static MpRespCode CODE_42001=new MpRespCode("42001","access_token超时，请检查access_token的有效期，请参考基础支持-获取access_token中，对access_token的详细机制说明");
	public static MpRespCode CODE_42002=new MpRespCode("42002","refresh_token超时");
	public static MpRespCode CODE_42003=new MpRespCode("42003","oauth_code超时");
	public static MpRespCode CODE_43001=new MpRespCode("43001","需要GET请求");
	public static MpRespCode CODE_43002=new MpRespCode("43002","需要POST请求");
	public static MpRespCode CODE_43003=new MpRespCode("43003","需要HTTPS请求");
	public static MpRespCode CODE_43004=new MpRespCode("43004","需要接收者关注");
	public static MpRespCode CODE_43005=new MpRespCode("43005","需要好友关系");
	public static MpRespCode CODE_44001=new MpRespCode("44001","多媒体文件为空");
	public static MpRespCode CODE_44002=new MpRespCode("44002","POST的数据包为空");
	public static MpRespCode CODE_44003=new MpRespCode("44003","图文消息内容为空");
	public static MpRespCode CODE_44004=new MpRespCode("44004","文本消息内容为空");
	public static MpRespCode CODE_45001=new MpRespCode("45001","多媒体文件大小超过限制");
	public static MpRespCode CODE_45002=new MpRespCode("45002","消息内容超过限制");
	public static MpRespCode CODE_45003=new MpRespCode("45003","标题字段超过限制");
	public static MpRespCode CODE_45004=new MpRespCode("45004","描述字段超过限制");
	public static MpRespCode CODE_45005=new MpRespCode("45005","链接字段超过限制");
	public static MpRespCode CODE_45006=new MpRespCode("45006","图片链接字段超过限制");
	public static MpRespCode CODE_45007=new MpRespCode("45007","语音播放时间超过限制");
	public static MpRespCode CODE_45008=new MpRespCode("45008","图文消息超过限制");
	public static MpRespCode CODE_45009=new MpRespCode("45009","接口调用超过限制");
	public static MpRespCode CODE_45010=new MpRespCode("45010","创建菜单个数超过限制");
	public static MpRespCode CODE_45015=new MpRespCode("45015","回复时间超过限制");
	public static MpRespCode CODE_45016=new MpRespCode("45016","系统分组，不允许修改");
	public static MpRespCode CODE_45017=new MpRespCode("45017","分组名字过长");
	public static MpRespCode CODE_45018=new MpRespCode("45018","分组数量超过上限");
	public static MpRespCode CODE_46001=new MpRespCode("46001","不存在媒体数据");
	public static MpRespCode CODE_46002=new MpRespCode("46002","不存在的菜单版本");
	public static MpRespCode CODE_46003=new MpRespCode("46003","不存在的菜单数据");
	public static MpRespCode CODE_46004=new MpRespCode("46004","不存在的用户");
	public static MpRespCode CODE_47001=new MpRespCode("47001","解析JSON/XML内容错误");
	public static MpRespCode CODE_48001=new MpRespCode("48001","api功能未授权，请确认公众号已获得该接口，可以在公众平台官网-开发者中心页中查看接口权限");
	public static MpRespCode CODE_50001=new MpRespCode("50001","用户未授权该api");
	public static MpRespCode CODE_50002=new MpRespCode("50002","用户受限，可能是违规后接口被封禁");
	public static MpRespCode CODE_61451=new MpRespCode("61451","参数错误(invalid parameter)");
	public static MpRespCode CODE_61452=new MpRespCode("61452","无效客服账号(invalid kf_account)");
	public static MpRespCode CODE_61453=new MpRespCode("61453","客服帐号已存在(kf_account exsited)");
	public static MpRespCode CODE_61454=new MpRespCode("61454","客服帐号名长度超过限制(仅允许10个英文字符，不包括@及@后的公众号的微信号)(invalid kf_acount length)");
	public static MpRespCode CODE_61455=new MpRespCode("61455","客服帐号名包含非法字符(仅允许英文+数字)(illegal character in kf_account)");
	public static MpRespCode CODE_61456=new MpRespCode("61456","客服帐号个数超过限制(10个客服账号)(kf_account count exceeded)");
	public static MpRespCode CODE_61457=new MpRespCode("61457","无效头像文件类型(invalid file type)");
	public static MpRespCode CODE_61450=new MpRespCode("61450","系统错误(system error)");
	public static MpRespCode CODE_61500=new MpRespCode("61500","日期格式错误");
	public static MpRespCode CODE_61501=new MpRespCode("61501","日期范围错误");
	public static MpRespCode CODE_9001001=new MpRespCode("9001001","POST数据参数不合法");
	public static MpRespCode CODE_9001002=new MpRespCode("9001002","远端服务不可用");
	public static MpRespCode CODE_9001003=new MpRespCode("9001003","Ticket不合法");
	public static MpRespCode CODE_9001004=new MpRespCode("9001004","获取摇周边用户信息失败");
	public static MpRespCode CODE_9001005=new MpRespCode("9001005","获取商户信息失败");
	public static MpRespCode CODE_9001006=new MpRespCode("9001006","获取OpenID失败");
	public static MpRespCode CODE_9001007=new MpRespCode("9001007","上传文件缺失");
	public static MpRespCode CODE_9001008=new MpRespCode("9001008","上传素材的文件类型不合法");
	public static MpRespCode CODE_9001009=new MpRespCode("9001009","上传素材的文件尺寸不合法");
	public static MpRespCode CODE_9001010=new MpRespCode("9001010","上传失败");
	public static MpRespCode CODE_9001020=new MpRespCode("9001020","帐号不合法");
	public static MpRespCode CODE_9001021=new MpRespCode("9001021","已有设备激活率低于50%，不能新增设备");
	public static MpRespCode CODE_9001022=new MpRespCode("9001022","设备申请数不合法，必须为大于0的数字");
	public static MpRespCode CODE_9001023=new MpRespCode("9001023","已存在审核中的设备ID申请");
	public static MpRespCode CODE_9001024=new MpRespCode("9001024","一次查询设备ID数量不能超过50");
	public static MpRespCode CODE_9001025=new MpRespCode("9001025","设备ID不合法");
	public static MpRespCode CODE_9001026=new MpRespCode("9001026","页面ID不合法");
	public static MpRespCode CODE_9001027=new MpRespCode("9001027","页面参数不合法");
	public static MpRespCode CODE_9001028=new MpRespCode("9001028","一次删除页面ID数量不能超过10");
	public static MpRespCode CODE_9001029=new MpRespCode("9001029","页面已应用在设备中，请先解除应用关系再删除");
	public static MpRespCode CODE_9001030=new MpRespCode("9001030","一次查询页面ID数量不能超过50");
	public static MpRespCode CODE_9001031=new MpRespCode("9001031","时间区间不合法");
	public static MpRespCode CODE_9001032=new MpRespCode("9001032","保存设备与页面的绑定关系参数错误");
	public static MpRespCode CODE_9001033=new MpRespCode("9001033","门店ID不合法");
	public static MpRespCode CODE_9001034=new MpRespCode("9001034","设备备注信息过长");
	public static MpRespCode CODE_9001035=new MpRespCode("9001035","设备申请参数不合法");
	public static MpRespCode CODE_9001036=new MpRespCode("9001036","查询起始值begin不合法");
	
	//标签
	public static MpRespCode CODE_45058=new MpRespCode("45058","不能修改0/1/2这三个系统默认保留的标签");
	public static MpRespCode CODE_45056=new MpRespCode("45056","创建的标签数过多，请注意不能超过100个");
	public static MpRespCode CODE_45057=new MpRespCode("45057","该标签下粉丝数超过10w，不允许直接删除");
	public static MpRespCode CODE_45157=new MpRespCode("45157","标签名非法，请注意不能和其他标签重名");
	public static MpRespCode CODE_45158=new MpRespCode("45158","标签名长度超过30个字节");
	public static MpRespCode CODE_45159=new MpRespCode("45159","非法的tag_id");
	public static MpRespCode CODE_45059=new MpRespCode("45059","有粉丝身上的标签数已经超过限制");
	public static MpRespCode CODE_49003=new MpRespCode("49003","传入的openid不属于此AppID");
	
	
	public static MpRespCode valueOf(String code){
		
		return ALL.get(code);
	}
	
	public MpRespCode(String code,String msg){
		this.code=code;this.msg=msg;
		ALL.put(code, this);
	}
	
	private String code;
	private String msg;

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}

}
