package com.jad.wx.mp.resp;

/**
 * 创建分组
 * @author hechuan
 *
 */
public class CreateGroupResp extends CommonResp{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private WxmpGroup group;

	public WxmpGroup getGroup() {
		return group;
	}

	public void setGroup(WxmpGroup group) {
		this.group = group;
	}

}
