package com.jad.wx.mp.service;

import com.jad.wx.mp.constant.WeixinMpException;
import com.jad.wx.mp.resp.CommonResp;
import com.jad.wx.mp.resp.GetKfAccountListResp;
import com.jad.wx.mp.resp.GetKfOnlineListResp;
import com.jad.wx.mp.resp.GetKfSessionListResp;
import com.jad.wx.mp.resp.GetKfSessionResp;
import com.jad.wx.mp.resp.GetKfWaitSessionListResp;
import com.jad.wx.mp.resp.GetKfmsglistResp;

/**
 * 新版客服接口
 * @author hechuan
 *
 */
public interface KfAccountService {
	
	/**
	 * 获取客服列表
	 * https://api.weixin.qq.com/cgi-bin/customservice/getkflist?access_token=ACCESS_TOKEN
	 * @param access_token
	 * @return
	 */
	GetKfAccountListResp getkflist(String access_token)throws WeixinMpException;
	
	/**
	 *  获取客服在线列表
	 * https://api.weixin.qq.com/cgi-bin/customservice/getonlinekflist?access_token=ACCESS_TOKEN
	 * @param access_token
	 * @return
	 */
	GetKfOnlineListResp getonlinekflist(String access_token)throws WeixinMpException;
	
	/**
	 * 添加客服号
	 * @param kf_account 完整客服帐号，格式为：帐号前缀@公众号微信号，帐号前缀最多10个字符，
	 * 					必须是英文、数字字符或者下划线，后缀为公众号微信号，长度不超过30个字符
	 * @param nickname 客服昵称，最长16个字
	 * @param access_token
	 * @return
	 * 返回码	说明
		0	成功
		65400	API不可用，即没有开通/升级到新客服功能
		65403	客服昵称不合法
		65404	客服帐号不合法
		65405	帐号数目已达到上限，不能继续添加
		65406	已经存在的客服帐号
	 * @throws WeixinMpException
	 */
	CommonResp add(String kf_account,String nickname,String access_token)throws WeixinMpException;
	
	/**
	 * 邀请绑定客服帐号
	 * @param kf_account 完整客服帐号，格式为：帐号前缀@公众号微信号
	 * @param invite_wx 接收绑定邀请的客服微信号
	 * @param access_token
	 * @return
	 * 返回码	说明
		0	成功
		65400	API不可用，即没有开通/升级到新版客服
		65401	无效客服帐号
		65407	邀请对象已经是本公众号客服
		65408	本公众号已发送邀请给该微信号
		65409	无效的微信号
		65410	邀请对象绑定公众号客服数量达到上限（目前每个微信号最多可以绑定5个公众号客服帐号）
		65411	该帐号已经有一个等待确认的邀请，不能重复邀请
		65412	该帐号已经绑定微信号，不能进行邀请
	 * @throws WeixinMpException
	 */
	CommonResp inviteworker(String kf_account,String invite_wx,String access_token)throws WeixinMpException;
	
	/**
	 * 设置客服信息
	 * https://api.weixin.qq.com/customservice/kfaccount/update?access_token=ACCESS_TOKEN
	 * @param kf_account 完整客服帐号，格式为：帐号前缀@公众号微信号
	 * @param nickname 客服昵称，最长16个字
	 * @param access_token
	 * @return
	 * 返回码	说明
		0	成功
		65400	API不可用，即没有开通/升级到新版客服功能
		65401	无效客服帐号
		65403	客服昵称不合法
	 * @throws WeixinMpException
	 */
	CommonResp update(String kf_account,String nickname,String access_token)throws WeixinMpException;
	
	/**
	 * 上传客服头像
	 * http请求方式: POST/FORM
		https://api.weixin.qq.com/customservice/kfaccount/uploadheadimg?access_token=ACCESS_TOKEN&kf_account=KFACCOUNT
		调用示例（使用curl命令，用FORM表单方式上传一个多媒体文件）：
	 * @param kf_account
	 * @param media
	 * @param access_token
	 * @return
	 * 返回码	说明
		0	成功
		65400	API不可用，即没有开通/升级到新版客服功能
		65401	无效客服帐号
		65403	客服昵称不合法
		40005	不支持的媒体类型
		40009	媒体文件长度不合法
	 * @throws WeixinMpException
	 */
	CommonResp uploadheadimg(String kf_account,String media,String access_token)throws WeixinMpException;
	
	/**
	 * 删除客服帐号
	 * https://api.weixin.qq.com/customservice/kfaccount/del?access_token=ACCESS_TOKEN&kf_account=KFACCOUNT
	 * @param kf_account
	 * @param access_token
	 * @return
	 * 返回码	 说明
		0	成功
		65400	API不可用，即没有开通/升级到新版客服功能
		65401	无效客服帐号
	 * @throws WeixinMpException
	 */
	CommonResp del(String kf_account, String access_token)throws WeixinMpException;
	
	/**
	 * 转发消息到客服
	 * @param toUserName 是	接收方帐号（收到的OpenID）
	 * @param fromUserName 是	开发者微信号
	 * @param createTime 是	消息创建时间 （整型）
	 * @param msgType 是	transfer_customer_service
	 * @param kfAccount  指定会话接入的客服账号
	 */
	void transferToCustomer(String toUserName, String fromUserName,String createTime, String msgType,String kfAccount)throws WeixinMpException;
	
	/**
	 * 创建客服会话
	 * https://api.weixin.qq.com/customservice/kfsession/create?access_token=ACCESS_TOKEN
	 * @param kf_account 完整客服帐号，格式为：帐号前缀@公众号微信号
	 * @param openid 粉丝的openid
	 * @param access_token 
	 * @return
	 */
	CommonResp createSession(String kf_account,String openid, String access_token)throws WeixinMpException;
	
	/**
	 * 关闭客服会话
	 * https://api.weixin.qq.com/customservice/kfsession/close?access_token=ACCESS_TOKEN
	 * @param kf_account 完整客服帐号，格式为：帐号前缀@公众号微信号
	 * @param openid 粉丝的openid
	 * @param access_token 
	 * @return
	 * 返回码	说明
		0	成功
		65400	API不可用，即没有开通/升级到新版客服功能
		65401       无效的客服帐号
		65402	帐号尚未绑定微信号，不能投入使用
		65413	不存在对应用户的会话信息
		65414	客户正在被其他客服接待
		40003	非法的openid
	 */
	CommonResp closeSession(String kf_account,String openid, String access_token)throws WeixinMpException;
	
	/**
	 * 获取客户会话状态
	 * https://api.weixin.qq.com/customservice/kfsession/getsession?access_token=ACCESS_TOKEN&openid=OPENID
	 * @param openid
	 * @param access_token
	 * @return
	 * 返回码	说明
		0	成功
		65400	API不可用，即没有开通/升级到新版客服功能
		40003	非法的openid
	 */
	GetKfSessionResp getKfSession(String openid, String access_token)throws WeixinMpException;
	
	/**
	 * 获取客服会话列表
	 * https://api.weixin.qq.com/customservice/kfsession/getsessionlist?access_token=ACCESS_TOKEN&kf_account=KFACCOUNT
	 * @param kf_account 完整客服帐号，格式为：帐号前缀@公众号微信号
	 * @param access_token
	 * @return
	 */
	GetKfSessionListResp GetKfSessionList(String kf_account , String access_token)throws WeixinMpException;
	
	/**
	 *  获取未接入会话列表
	 *  https://api.weixin.qq.com/customservice/kfsession/getwaitcase?access_token=ACCESS_TOKEN
	 * @param access_token
	 * @return
	 * 返回码	说明
		0	成功
		65400	API不可用，即没有开通或升级到新版客服功能
		65401	无效客服帐号
		65402	客服帐号尚未绑定微信号，不能投入使用
		65413	不存在对应用户的会话信息
		65414	粉丝正在被其他客服接待
		65415	指定的客服不在线
		40003	非法的openid
	 */
	GetKfWaitSessionListResp getKfWaitSessionList( String access_token)throws WeixinMpException;
	
	/**
	 * 获取客服聊天记录
	 * https://api.weixin.qq.com/customservice/msgrecord/getmsglist?access_token=ACCESS_TOKEN
	 * @param starttime 	起始时间，unix时间戳
	 * @param endtime 结束时间，unix时间戳，每次查询时段不能超过24小时
	 * @param msgid 消息id顺序从小到大，从1开始
	 * @param number 每次获取条数，最多10000条
	 * @param access_token
	 * @return
	 * 返回码	说明
		65400	API不可用，即没有开通或升级到新版客服功能
		65416	查询参数不合法
		65417	查询时间段超出限制
	 */
	GetKfmsglistResp getKfmsglist( long starttime, long endtime, long msgid, long number, String access_token)throws WeixinMpException;
	
}
