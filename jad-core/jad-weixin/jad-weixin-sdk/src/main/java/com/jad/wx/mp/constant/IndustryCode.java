package com.jad.wx.mp.constant;

import java.util.HashMap;
import java.util.Map;

/**
 * 行业代码
 * 
 * @author hechuan
 *
 */
public class IndustryCode {

	public static final Map<String, IndustryCode> ALL = new HashMap<String, IndustryCode>();

	public IndustryCode CODE_1 = new IndustryCode("IT科技", "互联网/电子商务", "1");
	public IndustryCode CODE_2 = new IndustryCode("IT科技", "IT软件与服务", "2");
	public IndustryCode CODE_3 = new IndustryCode("IT科技", "IT硬件与设备", "3");
	public IndustryCode CODE_4 = new IndustryCode("IT科技", "电子技术", "4");
	public IndustryCode CODE_5 = new IndustryCode("IT科技", "通信与运营商", "5");
	public IndustryCode CODE_6 = new IndustryCode("IT科技", "网络游戏", "6");
	public IndustryCode CODE_7 = new IndustryCode("金融业", "银行", "7");
	public IndustryCode CODE_8 = new IndustryCode("金融业", "基金|理财|信托", "8");
	public IndustryCode CODE_9 = new IndustryCode("金融业", "保险", "9");
	public IndustryCode CODE_10 = new IndustryCode("餐饮", "餐饮", "10");
	public IndustryCode CODE_11 = new IndustryCode("酒店旅游", "酒店", "11");
	public IndustryCode CODE_12 = new IndustryCode("酒店旅游", "旅游", "12");
	public IndustryCode CODE_13 = new IndustryCode("运输与仓储", "快递", "13");
	public IndustryCode CODE_14 = new IndustryCode("运输与仓储", "物流", "14");
	public IndustryCode CODE_15 = new IndustryCode("运输与仓储", "仓储", "15");
	public IndustryCode CODE_16 = new IndustryCode("教育", "培训", "16");
	public IndustryCode CODE_17 = new IndustryCode("教育", "院校", "17");
	public IndustryCode CODE_18 = new IndustryCode("政府与公共事业", "学术科研", "18");
	public IndustryCode CODE_19 = new IndustryCode("政府与公共事业", "交警", "19");
	public IndustryCode CODE_20 = new IndustryCode("政府与公共事业", "博物馆", "20");
	public IndustryCode CODE_21 = new IndustryCode("政府与公共事业", "公共事业|非盈利机构","21");
	public IndustryCode CODE_22 = new IndustryCode("医药护理", "医药医疗", "22");
	public IndustryCode CODE_23 = new IndustryCode("医药护理", "护理美容", "23");
	public IndustryCode CODE_24 = new IndustryCode("医药护理", "保健与卫生", "24");
	public IndustryCode CODE_25 = new IndustryCode("交通工具", "汽车相关", "25");
	public IndustryCode CODE_26 = new IndustryCode("交通工具", "摩托车相关", "26");
	public IndustryCode CODE_27 = new IndustryCode("交通工具", "火车相关", "27");
	public IndustryCode CODE_28 = new IndustryCode("交通工具", "飞机相关", "28");
	public IndustryCode CODE_29 = new IndustryCode("房地产", "建筑", "29");
	public IndustryCode CODE_30 = new IndustryCode("房地产", "物业", "30");
	public IndustryCode CODE_31 = new IndustryCode("消费品", "消费品", "31");
	public IndustryCode CODE_32 = new IndustryCode("商业服务", "法律", "32");
	public IndustryCode CODE_33 = new IndustryCode("商业服务", "会展", "33");
	public IndustryCode CODE_34 = new IndustryCode("商业服务", "中介服务", "34");
	public IndustryCode CODE_35 = new IndustryCode("商业服务", "认证", "35");
	public IndustryCode CODE_36 = new IndustryCode("商业服务", "审计", "36");
	public IndustryCode CODE_37 = new IndustryCode("文体娱乐", "传媒", "37");
	public IndustryCode CODE_38 = new IndustryCode("文体娱乐", "体育", "38");
	public IndustryCode CODE_39 = new IndustryCode("文体娱乐", "娱乐休闲", "39");
	public IndustryCode CODE_40 = new IndustryCode("印刷", "印刷", "40");
	public IndustryCode CODE_41 = new IndustryCode("其它", "其它", "41");

	public IndustryCode(String master, String vice, String code) {
		this.master = master;
		this.vice = vice;
		this.code = code;
	}

	private String master;// 主行业
	private String vice;// 副行业
	private String code; // 代码
}
