package com.jad.wx.mp.resp;

import java.util.List;

/**
 * 获得永久素材列表
 * @author hechuan
 *
 */
public class GetMaterialListResp  extends CommonResp{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String total_count;// 该类型的素材的总数
	private String item_count;//本次调用获取的素材的数量 
	
	private List<GetMaterialListItemResp>item;

	public String getTotal_count() {
		return total_count;
	}

	public void setTotal_count(String total_count) {
		this.total_count = total_count;
	}

	public String getItem_count() {
		return item_count;
	}

	public void setItem_count(String item_count) {
		this.item_count = item_count;
	}

	public List<GetMaterialListItemResp> getItem() {
		return item;
	}

	public void setItem(List<GetMaterialListItemResp> item) {
		this.item = item;
	}
	

}
