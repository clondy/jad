package com.jad.wx.mp.service;

import com.jad.wx.mp.constant.WeixinMpException;
import com.jad.wx.mp.req.CustomReq;
import com.jad.wx.mp.resp.CustomResp;
import com.jad.wx.mp.resp.KfListResp;

/**
 * 客服接口
 * @author hechuan
 *
 */
public interface CustomService {

	/**
	 * 添加客服帐号
	 * https://api.weixin.qq.com/customservice/kfaccount/add?access_token=ACCESS_TOKEN
	 * @param req
	 * @param accessToken
	 * @return
	 */
	CustomResp add(CustomReq req,String accessToken)throws WeixinMpException;
	
	/**
	 * 修改客服帐号
	 * https://api.weixin.qq.com/customservice/kfaccount/update?access_token=ACCESS_TOKEN
	 * @param req
	 * @param accessToken
	 * @return
	 */
	CustomResp update(CustomReq req,String accessToken)throws WeixinMpException;
	
	/**
	 * 删除客服帐号
	 * https://api.weixin.qq.com/customservice/kfaccount/update?access_token=ACCESS_TOKEN
	 * @param req
	 * @param accessToken
	 * @return
	 */
	CustomResp del(CustomReq req,String accessToken)throws WeixinMpException;
	
	
	/**
	 * 设置客服帐号的头像
	 * 开发者可调用本接口来上传图片作为客服人员的头像，头像图片文件必须是jpg格式，
	 * 推荐使用640*640大小的图片以达到最佳效果。该接口调用请求如下： 
	 * http://api.weixin.qq.com/customservice/kfaccount/uploadheadimg?access_token=ACCESS_TOKEN&kf_account=KFACCOUNT
	 * 调用示例：使用curl命令，用FORM表单方式上传一个多媒体文件，curl命令的具体用法请自行了解
	 * 
	 * @param req
	 * @param accessToken
	 * @return
	 */
	CustomResp uploadheadimg(String accessToken,String kfAccount)throws WeixinMpException;
	
	/**
	 * 获取所有客服账号
	 * https://api.weixin.qq.com/cgi-bin/customservice/getkflist?access_token=ACCESS_TOKEN
	 * @param req
	 * @param accessToken
	 * @return
	 */
	KfListResp getkflist(String accessToken)throws WeixinMpException;
	
}
