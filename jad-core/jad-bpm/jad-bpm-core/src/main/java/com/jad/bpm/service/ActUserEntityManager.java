/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.bpm.service;


/**
 * Activiti User Entity Service
 * @author ThinkGem
 * @version 2013-11-03
 */
public interface ActUserEntityManager  extends ActUserService{


//	public SystemService getSystemService() ;
//
//	public User createNewUser(String userId) ;
//
//	public void insertUser(User user) ;
//
//	public void updateUser(UserEntity updatedUser) ;
//
//	public UserEntity findUserById(String userId) ;
//
//	public void deleteUser(String userId) ;
//
//	public List<User> findUserByQueryCriteria(UserQueryImpl query, Page page) ;
//
//	public long findUserCountByQueryCriteria(UserQueryImpl query) ;
//
//	public List<Group> findGroupsByUser(String userId) ;
//
//	public UserQuery createNewUserQuery() ;
//
//	public IdentityInfoEntity findUserInfoByUserIdAndKey(String userId, String key) ;
//
//	public List<String> findUserInfoKeysByUserIdAndType(String userId, String type) ;
//
//	public Boolean checkPassword(String userId, String password) ;
//
//	public List<User> findPotentialStarterUsers(String proceDefId) ;
//
//	public List<User> findUsersByNativeQuery(Map<String, Object> parameterMap, int firstResult, int maxResults) ;
//
//	public long findUserCountByNativeQuery(Map<String, Object> parameterMap) ;
	
}
