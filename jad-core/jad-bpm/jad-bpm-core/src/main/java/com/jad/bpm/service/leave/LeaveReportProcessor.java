package com.jad.bpm.service.leave;

import org.activiti.engine.delegate.DelegateTask;

/**
 * 销假后处理器
 * @author liuj
 */
public interface LeaveReportProcessor   {
	
	/**
	 * 销假完成后执行，保存实际开始和结束时间
	 */
	public void notify(DelegateTask delegateTask) ;

}
